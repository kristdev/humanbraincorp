/**
 * Active l'ouverture, la cloture et l'expansion des boites ibox
 */
function iboxtools_enable (){
	// Collapse ibox function
	$('.collapse-link').on('click', function (e) {
	    e.preventDefault();
	    var ibox = $(this).closest('div.ibox');
	    var button = $(this).find('i');
	    var content = ibox.children('.ibox-content');
	    content.slideToggle(200);
	    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
	    ibox.toggleClass('').toggleClass('border-bottom');
	    setTimeout(function () {
	        ibox.resize();
	        ibox.find('[id^=map-]').resize();
	    }, 50);
	});

	// Close ibox function
	$('.close-link').on('click', function (e) {
	    e.preventDefault();
	    var content = $(this).closest('div.ibox');
	    content.remove();
	});

	// Fullscreen ibox function
	$('.fullscreen-link').on('click', function (e) {
	    e.preventDefault();
	    var ibox = $(this).closest('div.ibox');
	    var button = $(this).find('i');
	    $('body').toggleClass('fullscreen-ibox-mode');
	    button.toggleClass('fa-expand').toggleClass('fa-compress');
	    ibox.toggleClass('fullscreen');
	    setTimeout(function () {
	        $(window).trigger('resize');
	    }, 100);
	});
};

// First loading
up.compiler('body', function(){
	init_tpl_features();
});

// Deconnexion 1
up.compiler('.logout', function(){
    $('.logout').click(function () {
        swal({
            title: "Are you sure?",
            text: "You will close your session",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, close it",
            closeOnConfirm: false
        }, function () {
            console.log('Session closed');
            window.location.replace('http://localhost/humanbraincorp/myprivate/auth/logout');
        });
    });
})

// Deconnexion 2
up.compiler('.logout2', function(){
    $('.logout2').click(function () {
        swal({
            title: "Are you sure?",
            text: "You will close your session",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, close it",
            closeOnConfirm: false
        }, function () {
            console.log('Session closed');
            window.location.replace('http://localhost/humanbraincorp/myprivate/auth/logout');
        });
    });
})

// flot-dashboard-chart
up.compiler('#flot-dashboard-chart', function(){
	var data1 = [
	    [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
	];
	var data2 = [
	    [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
	];
	$("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
	    data1, data2
	],
	        {
	            series: {
	                lines: {
	                    show: false,
	                    fill: true
	                },
	                splines: {
	                    show: true,
	                    tension: 0.4,
	                    lineWidth: 1,
	                    fill: 0.4
	                },
	                points: {
	                    radius: 0,
	                    show: true
	                },
	                shadowSize: 2
	            },
	            grid: {
	                hoverable: true,
	                clickable: true,
	                tickColor: "#d5d5d5",
	                borderWidth: 1,
	                color: '#d5d5d5'
	            },
	            colors: ["#1ab394", "#1C84C6"],
	            xaxis:{
	            },
	            yaxis: {
	                ticks: 4
	            },
	            tooltip: false
	        }
	);
});

// Dognout chart
up.compiler('#doughnutChart', function(){
	var doughnutData = {
	    labels: ["App","Software","Laptop" ],
	    datasets: [{
	        data: [300,50,100],
	        backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
	    }]
	} ;


	var doughnutOptions = {
	    responsive: false,
	    legend: {
	        display: false
	    }
	};


	var ctx4 = document.getElementById("doughnutChart").getContext("2d");
	new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
});

// Doughnutchart2
up.compiler('#doughnutChart2', function(){
	var doughnutData = {
	    labels: ["App","Software","Laptop" ],
	    datasets: [{
	        data: [70,27,85],
	        backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
	    }]
	} ;


	var doughnutOptions = {
	    responsive: false,
	    legend: {
	        display: false
	    }
	};


	var ctx4 = document.getElementById("doughnutChart2").getContext("2d");
	new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
});

// Page Dashboard js templates
up.compiler('#pagedashboard', function(){
	iboxtools_enable();
});

// Page profile js templates
up.compiler('#pageprofil', function(){
	iboxtools_enable();
	riot.mount('portfoliotag');
	riot.mount('infoprofiletag');
	$('.chosen-select').chosen({width: "100%"});
	$('.chosen-select-paysexercice').chosen({width: "100%"});
});