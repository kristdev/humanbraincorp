riot.tag2('infoprofiletag', '<div> <div class="ibox-content no-padding border-left-right" style="position: relative;"> <img alt="image" class="img-fluid" riot-src="{opts.base_url}/public/img/covers/default.jpg"> <div style="position: absolute; right: 5%; bottom:5%; display: inline-block;"> <button class="btn btn-default btn-circle btn-lg editpicts" type="button" title="Editer la couverture et la photo de profil"><i class="fa fa-picture-o"></i></button> <button class="btn btn-default btn-circle btn-lg editinfos" type="button" title="Editer les infos du profil"><i class="fa fa-edit"></i></button> </div> </div> <div class="ibox-content profile-content"> <infoprofileshowinfos show="{editform === false && editpicts === false}" base_url="{opts.base_url}" email="{opts.email}" username="{opts.username}" name="{opts.name}" address="{opts.address}" biographie="{opts.biographie}" residence="{opts.residence}" mobiles="{opts.mobiles}" type_compte="{opts.type_compte}" pays_exercice="{opts.pays_exercice}" metiers="{opts.metiers}"></infoprofileshowinfos> <infoprofileformeditinfos show="{editform === true}" base_url="{opts.base_url}" userid="{opts.userid}" username="{opts.username}" email="{opts.email}" countries="{opts.countries}" secteurarray="{opts.secteurarray}" name="{opts.name}" address="{opts.address}" biographie="{opts.biographie}" residence="{opts.residence}" mobiles="{opts.mobiles}" type_compte="{opts.type_compte}" pays_exercice="{opts.pays_exercice}" metiers="{opts.metiers}"></infoprofileformeditinfos> <infoprofileeditpicts show="{editpicts === true}" base_url="{opts.base_url}"></infoprofileeditpicts> </div> </div>', '', '', function(opts) {
		this.editform = false;
        this.editpicts = false;

		var self = this;

		this.on('mount', function(){
			document.querySelector('.editinfos').addEventListener('click', function(){
				self.editform = !self.editform;
                self.update();
			});

            document.querySelector('.editpicts').addEventListener('click', function(){
                self.editpicts = !self.editpicts;
                self.update();
            });
		});

});
riot.tag2('infoprofileeditpicts', '<div class="tabs-container"> <ul class="nav nav-tabs" role="tablist"> <li><a class="nav-link active" data-toggle="tab" href="#tab-1">Photo de profil</a></li> <li><a class="nav-link" data-toggle="tab" href="#tab-2">Image de couverture</a></li> </ul> <div class="tab-content"> <div role="tabpanel" id="tab-1" class="tab-pane active"> <div class="panel-body bg-muted"> <div class="text-center"> <div class="text-center" style="width: 100%"> <img alt="image" class="img-fluid theimage" riot-src="{opts.base_url}/public/img/profiles/default.jpg"> </div> <div class="clearfix" style="height: 20px;"></div> <div class="form-group"> <input type="file" placeholder="Choisissez votre photo de profil" class="form-control"> </div> <div class="text-center imgpreview" style="width: 100%"> <img alt="image" class="rounded-circle img-fluid" riot-src="{opts.base_url}/public/img/profiles/default.jpg"> </div> <div class="clearfix" style="height: 10px;"></div> <button class="btn btn-info">Modifier la photo de profil</button> </div> </div> </div> <div role="tabpanel" id="tab-2" class="tab-pane bg-muted"> <div class="panel-body"> <div class="text-center"> <img alt="image" class="img-fluid" riot-src="{opts.base_url}/public/img/covers/default.jpg"> <div class="clearfix" style="height: 10px;"></div> <button class="btn btn-info" data-toggle="modal" data-target="#updtcover">Modifier la photo de couverture</button> <div class="modal inmodal" id="updtcover" tabindex="-1" role="dialog" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content animated bounceInRight"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <i class="fa fa-laptop modal-icon"></i> <h4 class="modal-title">Modal title</h4> <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> </div> <div class="modal-body"> <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p> <div class="form-group"><label>Sample Input</label> <input placeholder="Enter your email" class="form-control" type="email"></div> </div> <div class="modal-footer"> <button type="button" class="btn btn-white" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button> </div> </div> </div> </div> </div> </div> </div> </div> </div>', 'infoprofileeditpicts .imgpreview,[data-is="infoprofileeditpicts"] .imgpreview{ overflow: hidden; height: 192px; width: 192px; } infoprofileeditpicts img,[data-is="infoprofileeditpicts"] img{ display: block; margin-left: auto; margin-right: auto; }', '', function(opts) {
		var self = this;
		this.on('mount', function(){
			var theimage = document.querySelector('.theimage');
			console.log(theimage);
			$(theimage).cropper({
	            aspectRatio: 1,
	            preview: ".imgpreview",
	            done: function(data) {

	                console.log('test cropper');
	            }
	        });
		})

});
riot.tag2('infoprofileformeditinfos', '<div class="formeditinfos"> <form method="post" id="forminfoprofile"> <div class="form-group"> <label><i class="fa fa-user"></i> Username</label> <input type="text" placeholder="Usernme" class="form-control" disabled="" riot-value="{opts.username}"> </div> <div class="form-group"> <label><i class="fa fa-user"></i> Nom complet</label> <input type="text" class="form-control" ref="nomcomplet" riot-value="{opts.name}"> </div> <div class="form-group"> <label><i class="fa fa-address-card-o"></i> A propos</label> <textarea rows="3" class="form-control" ref="biographie" riot-value="{opts.biographie}"></textarea> </div> <div class="form-group"> <label><i class="fa fa-flag"></i> Pays de résidence</label> <select class="form-control m-b" name="pays_residence" ref="residence"> <option each="{country in opts.countries}" riot-value="{country.id}" selected="{opts.residence.includes(country.id) == true}">{country.nicename}</option> </select> </div> <div class="form-group"> <label><i class="fa fa-map-marker"></i> Adresse</label> <input type="text" placeholder="Adresse" class="form-control" ref="address" riot-value="{opts.address}"> </div> <div class="form-group"> <label><i class="fa fa-envelope"></i> Email</label> <input type="text" placeholder="Adresse" class="form-control" disabled="" riot-value="{opts.email}"> </div> <div class="form-group parentblock-tel"> <label><i class="fa fa-mobile"></i> Mobile</label> <div class="input-group"> <input type="text" placeholder="Numéro mobile" class="form-control" ref="mobiles" riot-value="{opts.mobiles}"> </div> </div> <div class="hr-line-dashed"></div> <div class="form-group"> <label><i class="fa fa-user"></i> Compte</label> <select class="form-control m-b" name="type_compte" ref="type_compte"> <option value="Individuel">Individuel</option> <option value="Organisation">Organisation</option> </select> </div> <div class="form-group"> <label><i class="fa fa-map-flag"></i> Pays d\'exercices</label> <select class="form-control m-b chosen-select-paysexercice" name="pays_execrice" multiple="" ref="pays_exercice"> <option each="{country in opts.countries}" riot-value="{country.id}" selected="{opts.pays_exercice.split(\'|\').includes(country.id) == true}">{country.nicename}</option> </select> </div> <div class="form-group"> <label><i class="fa fa-map-flag"></i> Métiers</label> <select class="form-control m-b chosen-select" name="metiers" multiple="" ref="metiers"> <optgroup label="{metier.secteur}" each="{metier in opts.secteurarray}"> <option riot-value="{unmetier.id}" each="{unmetier in metier.metiers}" selected="{opts.metiers.split(\'|\').includes(unmetier.id) == true}">{unmetier.libelle}</option> </optgroup> </select> </div> <div class="form-group text-center"> <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Enregistrer les informations du profile</button> </div> </form> </div>', '', '', function(opts) {
		var self = this;
		this.on('mount', function(){
			var form = document.querySelector('#forminfoprofile');
			form.onsubmit=function(e){
				e.preventDefault();
				var paysexercicesArray = [];
				var elpaysexercice = Array.from(self.refs.pays_exercice.selectedOptions);
				elpaysexercice.forEach(function(e){
					paysexercicesArray.push(e.value);
				});
				var metiersArray = [];
				var elmetiers = Array.from(self.refs.metiers.selectedOptions);
				elmetiers.forEach(function(e){
					metiersArray.push(e.value);
				});
				var dataobj = {
					id: opts.userid,
					name: self.refs.nomcomplet.value,
					biographie: self.refs.biographie.value,
					residence: self.refs.residence.value,
					address: self.refs.address.value,
					mobiles: self.refs.mobiles.value,
					type_compte: self.refs.type_compte.value,
					pays_exercice: paysexercicesArray.join("|"),
					metiers: metiersArray.join("|")
				}

				var query = $.param(dataobj);
				console.log(opts.base_url+'api/user/update-user?'+query);
				$.get(opts.base_url+'api/user/update-user?'+query, function(data, status){
			      if(data['response'] == 'Success'){
			      	swal({
			      	        title: "Confirmez vous?",
			      	        text: "Vous souhaiter vraiment modifier votre profil?",
			      	        type: "warning",
			      	        showCancelButton: true,
			      	        confirmButtonColor: "#1ab394",
			      	        confirmButtonText: "Oui, modifier!",
			      	        closeOnConfirm: false
			      	    }, function () {
			      	        swal({
			      	        	title: "Good Job!",
			      	        	text: "Vous avez modifié votre profil",
			      	        	type: "success"
			      	        }, function(){
			      	        	location.reload(true);
			      	        });
			      	    });
			      }else{

			      }
			    });
			}
		})
});
riot.tag2('infoprofileshowinfos', '<div class="showinfos"> <div class="text-center"> <img alt="image" class="rounded-circle" riot-src="{opts.base_url}/public/img/profiles/default.jpg" style="height: 100px"> <h4><strong>{opts.name}</strong> ({opts.username})</h4> <p><i class="fa fa-map-marker"></i> {opts.address} </p> </div> <h5><abbr title="A propos de moi"><strong>Biographie:</strong></abbr></h5> <p> {opts.biographie} </p> <h5><abbr title="Pays de résidence"><strong>Résidence:</strong></abbr></h5> <p show="{opts.residence != ⁗Résidence à pourvoir⁗}"> <span each="{elresidence in residence}"><img class="flag-icon flag-icon-squared border" riot-src="{opts.base_url}/public/assets/flags/svg/{elresidence[0].iso}.svg" alt="{elresidence[0].nicename}"> {elresidence[0].nicename}</span> </p> <p show="{opts.residence === ⁗Résidence à pourvoir⁗}"><span>{opts.residence}</p> <h5><abbr title="Numéros de téléphone"><strong>Mobiles:</strong></abbr></h5> <p show="{opts.mobiles != ⁗Numéros mobiles à pourvoir⁗}"> <span each="{mobile in mobiles}">{mobile} </span> </p> <p show="{opts.mobiles === ⁗Numéros mobiles à pourvoir⁗}"> {opts.mobiles} </p> <h5><abbr title="Email contact"><strong>Email:</strong></abbr></h5> <p> <span>{opts.email}</span> </p> <h5><abbr title="Type de compte"><strong>Compte:</strong></abbr></h5> <p> <span>{opts.type_compte}</span> </p> <h5><abbr title="Pays d\'exercices"><strong>Zones d\'action:</strong></abbr></h5> <p show="{opts.pays_exercice != ⁗Pays d\'exercices à définir⁗}"> <span each="{country in pays_exercice}"><img class="flag-icon flag-icon-squared border" riot-src="{opts.base_url}/public/assets/flags/svg/{country[0].iso}.svg" alt="{country[0].nicename}"> {country[0].nicename} </span> </p> <p show="{opts.pays_exercice === ⁗Pays d\'exercices à définir⁗}">{opts.pays_exercice}</p> <h5><abbr title="Secteurs d\'activités"><strong>Métiers:</strong></abbr></h5> <p show="{opts.metiers != ⁗Métiers à pourvoir⁗}"> <span each="{metier, i in metiers}">{metier[0].libelle} | </span> </p> <p show="{opts.metiers === ⁗Métiers à pourvoir⁗}">{opts.metiers}</p> </div>', '', '', function(opts) {
		var self = this;

		if(opts.mobiles != "Numéros mobiles à pourvoir"){
			this.mobiles = opts.mobiles
		}

		if(opts.address != "Numéros mobiles à pourvoir"){
			this.address = opts.address
		}

		if(opts.pays_exercice != "Pays d'exercices à définir"){
			var dataobj = {
				chainepays: opts.pays_exercice
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-pays?'+query, function(data, status){
			      self.pays_exercice = data;
			      self.update();
			});
		}

		if(opts.residence != "Résidence à pourvoir"){
			var dataobj = {
				chainepays: opts.residence
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-pays?'+query, function(data, status){
			      self.residence = data;

			      self.update();
			});
		}

		if(opts.metiers != "Métiers à pourvoir"){
			var dataobj = {
				chainemetiers: opts.metiers
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-metiers?'+query, function(data, status){
			      self.metiers = data;

			      self.update();
			});
		}
});
riot.tag2('portfoliotag', '<div class="feed-activity-list"> <div class=""> <form method="POST" class="addexperience"> <div class="form-group row"><label class="col-sm-2 col-form-label">Libellé</label> <div class="col-sm-10"><input type="text" class="form-control" placeholder="Formuez le libellé de l\'expérience" ref="libelle" required=""></div> </div> <div class="form-group row"><label class="col-sm-2 col-form-label">Date</label> <div class="col-sm-10"><input type="text" class="form-control dateexperience" placeholder="Sélectionnez la date" ref="date" required=""></div> </div> <div class="form-group row"> <label class="col-sm-2 col-form-label"> Description <br> <span class="text-warning">[- <span class="remaining">{remaining} </span> caractères]</span> </label> <div class="col-sm-10"> <textarea name="" id="" rows="3" class="form-control" placeholder="Donne la description de votre réalisation ou de l\'expérience sollicitée..." ref="description" required="" maxlength="450"></textarea> </div> </div> <div class="text-center"> <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Ajouter une expérience</button> <button class="btn" type="reset">Annuler</button> </div> </form> <div class="hr-line-dashed"></div> <div class="text-center nodata" if="{portfolioelts.length === 0}"> <br> <h2 style="width:500px; margin: auto">Il n\'y a actuellement aucune données disponible veuillez remplir de formulaire ci-dessus pour ajouter une expérience</h2> <img riot-src="{opts.base_url}public/img/box.svg" alt="empty box" height="400"> <br> </div> </div> <div class="portfoliolist" if="{portfolioelts.length != 0}"> <div class="feed-element animate fadeIn" each="{portfoliotelt, i in portfolioelts}"> <a href="#" class="float-left"> <img alt="image" class="rounded-circle" riot-src="{opts.base_url}public/templates/01/img/a1.jpg"> </a> <div class="media-body "> <small class="float-right tempsecoule">{showtempsecoule(portfoliotelt.date_creation)}</small> <strong>{portfoliotelt.libelle}</strong> <br> <small class="text-muted">{portfoliotelt.date}</small> <div class="well"> {portfoliotelt.description} </div> <div class="float-right"> <a href="javascript:;" class="btn btn-xs btn-white" onclick="{editexperience}"><i class="fa fa-edit"></i> Edit </a> <a href="javascript:;" class="btn btn-xs btn-info" onclick="{}"><i class="fa fa-file-picture-o"></i> Images</a> <a href="javascript:;" class="btn btn-xs btn-info" onclick="{}"><i class="fa fa-link"></i> Links</a> <a href="javascript:;" class="btn btn-xs btn-danger" onclick="{deleteexperience}"><i class="fa fa-trash"></i> Delete</a> </div> </div> </div> </div> </div>', '', '', function(opts) {
		this.portfolioelts = [];
		this.remaining = 450;
		var self = this;

		this.showtempsecoule = function(temps){
			return jQuery.timeago(temps);
		};

		this.deleteexperience = function(){
		    swal({
		        title: "Etes vous sûr?",
		        text: "Cette action est irreversible, vous perdrez cette expérience!",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes, delete it!",
		        closeOnConfirm: false
		    }, function () {
		        swal("Deleted!", "Expérience supprimée avec succès.", "success");
				self.portfolioelts.splice(self.i, 1);
				console.log(this.portfolioelts);
				self.update();
		    });
		};

		this.on('mount', function(){
			const formAdd = document.querySelector('.addexperience');

			formAdd.addEventListener('submit', function(e){
				e.preventDefault();
				let obj = {
					libelle: self.refs.libelle.value,
					date: self.refs.date.value,
					description: self.refs.description.value,
					date_creation: new Date().getTime()
				};
				self.portfolioelts.unshift(obj);
				self.refs.libelle.value = "";
				self.refs.date.value = "";
				self.refs.description.value = "";
				self.remaining = self.refs.description.maxLength;
				document.querySelector('.remaining').textContent = self.remaining;
				self.update();
				console.log(self.portfolioelts);
			});

			self.refs.description.addEventListener('input', function(){
				self.remaining = self.refs.description.maxLength - self.refs.description.value.length*1;
				document.querySelector('.remaining').textContent = self.remaining;
			})

			$('.dateexperience').datepicker({
		        startView: 1,
		        todayBtn: "linked",
		        keyboardNavigation: false,
		        forceParse: false,
		        autoclose: true,
		        format: "dd/mm/yyyy"
		    });
		});
});