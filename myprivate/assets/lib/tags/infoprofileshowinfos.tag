<infoprofileshowinfos>
	<div class="showinfos">
		<div class="text-center">
			<img alt="image" class="rounded-circle" src="{opts.base_url}/public/img/profiles/default.jpg" style="height: 100px">
		    <h4><strong>{opts.name}</strong> ({opts.username})</h4>
		    <p><i class="fa fa-map-marker"></i> {opts.address} </p>
		</div>

	    <h5><abbr title="A propos de moi"><strong>Biographie:</strong></abbr></h5>
	    <p>
	        {opts.biographie}
	    </p>

	    <h5><abbr title="Pays de résidence"><strong>Résidence:</strong></abbr></h5>
	    <p show="{opts.residence != "Résidence à pourvoir"}">
	    	<span each="{elresidence in residence}"><img class="flag-icon flag-icon-squared border" src="{opts.base_url}/public/assets/flags/svg/{elresidence[0].iso}.svg" alt="{elresidence[0].nicename}"> {elresidence[0].nicename}</span>
	    </p>
	    <p show={opts.residence === "Résidence à pourvoir"}><span>{opts.residence}</p>
	    
	    <h5><abbr title="Numéros de téléphone"><strong>Mobiles:</strong></abbr></h5>
		<p show={opts.mobiles != "Numéros mobiles à pourvoir"}>
			<span each="{mobile in mobiles}">{mobile} </span>
	    </p>
	    <p show={opts.mobiles === "Numéros mobiles à pourvoir"}>
	    	{opts.mobiles}
	    </p>

	    <h5><abbr title="Email contact"><strong>Email:</strong></abbr></h5>
	    <p>
	    	<span>{opts.email}</span>
	    </p>

	    <h5><abbr title="Type de compte"><strong>Compte:</strong></abbr></h5>
	    <p>
	    	<span>{opts.type_compte}</span>
	    </p>

	    <h5><abbr title="Pays d'exercices"><strong>Zones d'action:</strong></abbr></h5>
	    <p show={opts.pays_exercice != "Pays d'exercices à définir"}>
	    	<span each="{country in pays_exercice}"><img class="flag-icon flag-icon-squared border" src="{opts.base_url}/public/assets/flags/svg/{country[0].iso}.svg" alt="{country[0].nicename}"> {country[0].nicename} </span>
	    </p>
	    <p show={opts.pays_exercice === "Pays d'exercices à définir"}>{opts.pays_exercice}</p>

	    <h5><abbr title="Secteurs d'activités"><strong>Métiers:</strong></abbr></h5>
	    <p show={opts.metiers != "Métiers à pourvoir"}>
	    	<span each="{metier, i in metiers}">{metier[0].libelle} |  </span>
	    </p>
	    <p show={opts.metiers === "Métiers à pourvoir"}>{opts.metiers}</p>
	</div>

	<script>
		var self = this;

		if(opts.mobiles != "Numéros mobiles à pourvoir"){
			this.mobiles = opts.mobiles
		}

		if(opts.address != "Numéros mobiles à pourvoir"){
			this.address = opts.address
		}

		if(opts.pays_exercice != "Pays d'exercices à définir"){
			var dataobj = {
				chainepays: opts.pays_exercice
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-pays?'+query, function(data, status){
			      self.pays_exercice = data;
			      self.update();
			});
		}

		if(opts.residence != "Résidence à pourvoir"){
			var dataobj = {
				chainepays: opts.residence
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-pays?'+query, function(data, status){
			      self.residence = data;
			      // console.log(self.residence);
			      self.update();
			});
		}

		if(opts.metiers != "Métiers à pourvoir"){
			var dataobj = {
				chainemetiers: opts.metiers
			};
			var query = $.param(dataobj);
			$.get(opts.base_url+'api/user/read-metiers?'+query, function(data, status){
			      self.metiers = data;
			      // console.log(self.metiers);
			      self.update();
			});
		}
	</script>
</infoprofileshowinfos>