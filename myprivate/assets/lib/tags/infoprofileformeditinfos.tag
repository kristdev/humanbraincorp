<infoprofileformeditinfos>
	<div class="formeditinfos">
	    <form method="post" id="forminfoprofile">
	        <div class="form-group">
	            <label><i class="fa fa-user"></i> Username</label> 
	            <input type="text" placeholder="Usernme" class="form-control" disabled="" value="{opts.username}">
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-user"></i> Nom complet</label> 
	            <input type="text" class="form-control" ref="nomcomplet" value="{opts.name}">
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-address-card-o"></i> A propos</label> 
	            <textarea rows="3" class="form-control" ref="biographie" value="{opts.biographie}"></textarea>
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-flag"></i> Pays de résidence</label> 
	            <select class="form-control m-b" name="pays_residence" ref="residence">
	                <option each="{country in opts.countries}" value="{country.id}" selected={opts.residence.includes(country.id) == true}>{country.nicename}</option>
	            </select>
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-map-marker"></i> Adresse</label> 
	            <input type="text" placeholder="Adresse" class="form-control" ref="address" value="{opts.address}">
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-envelope"></i> Email</label> 
	            <input type="text" placeholder="Adresse" class="form-control" disabled="" value="{opts.email}">
	        </div>
	        <div class="form-group parentblock-tel">
	            <label><i class="fa fa-mobile"></i> Mobile</label>
	            <div class="input-group">
	                <input type="text" placeholder="Numéro mobile" class="form-control" ref="mobiles" value="{opts.mobiles}">
	                <!-- <span class="input-group-append"> 
	                    <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button> 
	                </span> -->
	            </div>
	        </div>
	        <div class="hr-line-dashed"></div>
	        <div class="form-group">
	            <label><i class="fa fa-user"></i> Compte</label> 
	            <select class="form-control m-b" name="type_compte" ref="type_compte">
	                <option value="Individuel">Individuel</option>
	                <option value="Organisation">Organisation</option>
	            </select>
	        </div>
	        <div class="form-group">
	            <label><i class="fa fa-map-flag"></i> Pays d'exercices</label> 
	            <select class="form-control m-b chosen-select-paysexercice" name="pays_execrice" multiple="" ref="pays_exercice">
	                <option each="{country in opts.countries}" value="{country.id}" selected={opts.pays_exercice.split('|').includes(country.id) == true}>{country.nicename}</option>
	            </select>
	        </div>
	        <!-- <div class="form-group"
	            <label><i class="fa fa-map-flag"></i> Secteurs d'activités</label> 
	            <select class="form-control m-b chosen-select" name="pays_execrice" multiple="">
	                <option value="Individuel">Individuel</option>
	                <option value="Organisation">Organisation</option>
	            </select>
	        </div> -->
	        <div class="form-group">
	            <label><i class="fa fa-map-flag"></i> Métiers</label> 
	            <select class="form-control m-b chosen-select" name="metiers" multiple="" ref="metiers">
	            	<optgroup label="{metier.secteur}" each="{metier in opts.secteurarray}">
	            		<option value="{unmetier.id}" each="{unmetier in metier.metiers}"  selected={opts.metiers.split('|').includes(unmetier.id) == true}>{unmetier.libelle}</option>
	            	</optgroup>
	            </select>
	        </div>
	        <div class="form-group text-center">
	            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Enregistrer les informations du profile</button> 
	        </div>
	    </form>
	</div>

	<script>
		var self = this;
		this.on('mount', function(){
			var form = document.querySelector('#forminfoprofile');
			form.onsubmit=function(e){
				e.preventDefault();
				var paysexercicesArray = [];
				var elpaysexercice = Array.from(self.refs.pays_exercice.selectedOptions);
				elpaysexercice.forEach(function(e){
					paysexercicesArray.push(e.value);
				});
				var metiersArray = [];
				var elmetiers = Array.from(self.refs.metiers.selectedOptions);
				elmetiers.forEach(function(e){
					metiersArray.push(e.value);
				});
				var dataobj = {
					id: opts.userid,
					name: self.refs.nomcomplet.value,
					biographie: self.refs.biographie.value,
					residence: self.refs.residence.value,
					address: self.refs.address.value,
					mobiles: self.refs.mobiles.value,
					type_compte: self.refs.type_compte.value,
					pays_exercice: paysexercicesArray.join("|"),
					metiers: metiersArray.join("|")
				}

				var query = $.param(dataobj);
				console.log(opts.base_url+'api/user/update-user?'+query);
				$.get(opts.base_url+'api/user/update-user?'+query, function(data, status){
			      if(data['response'] == 'Success'){
			      	swal({
			      	        title: "Confirmez vous?",
			      	        text: "Vous souhaiter vraiment modifier votre profil?",
			      	        type: "warning",
			      	        showCancelButton: true,
			      	        confirmButtonColor: "#1ab394",
			      	        confirmButtonText: "Oui, modifier!",
			      	        closeOnConfirm: false
			      	    }, function () {
			      	        swal({
			      	        	title: "Good Job!", 
			      	        	text: "Vous avez modifié votre profil", 
			      	        	type: "success"
			      	        }, function(){
			      	        	location.reload(true);
			      	        });
			      	    });
			      }else{
			      	// console.log('error')
			      }
			    });
			}
		})
	</script>
</infoprofileformeditinfos>