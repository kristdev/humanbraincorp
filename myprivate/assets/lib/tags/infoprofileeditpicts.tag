<infoprofileeditpicts>
	<div class="tabs-container">
		<ul class="nav nav-tabs" role="tablist">
			<li><a class="nav-link active" data-toggle="tab" href="#tab-1">Photo de profil</a></li>
			<li><a class="nav-link" data-toggle="tab" href="#tab-2">Image de couverture</a></li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" id="tab-1" class="tab-pane active">
				<div class="panel-body bg-muted">
					<div class="text-center">
						<div class="text-center" style="width: 100%">
                        	<img alt="image" class="img-fluid theimage" src="{opts.base_url}/public/img/profiles/default.jpg">
                        </div>
                        <div class="clearfix" style="height: 20px;"></div>
						<div class="form-group">
							<!-- <label>Choix de votre photo de profil</label>  -->
							<input type="file" placeholder="Choisissez votre photo de profil" class="form-control">
						</div>
						<div class="text-center imgpreview" style="width: 100%">
							<img alt="image" class="rounded-circle img-fluid" src="{opts.base_url}/public/img/profiles/default.jpg">
						</div>
						<div class="clearfix" style="height: 10px;"></div>
						<button class="btn btn-info">Modifier la photo de profil</button>
					</div>
				</div>
			</div>
			<div role="tabpanel" id="tab-2" class="tab-pane bg-muted">
				<div class="panel-body">
					<div class="text-center">
						<img alt="image" class="img-fluid" src="{opts.base_url}/public/img/covers/default.jpg">
						<div class="clearfix" style="height: 10px;"></div>
						<button class="btn btn-info" data-toggle="modal" data-target="#updtcover">Modifier la photo de couverture</button>
						<div class="modal inmodal" id="updtcover" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
								<div class="modal-content animated bounceInRight">
		                            <div class="modal-header">
		                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		                                <i class="fa fa-laptop modal-icon"></i>
		                                <h4 class="modal-title">Modal title</h4>
		                                <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
		                            </div>
		                            <div class="modal-body">
		                                <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
		                                    printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
		                                    remaining essentially unchanged.</p>
		                                        <div class="form-group"><label>Sample Input</label> <input type="email" placeholder="Enter your email" class="form-control"></div>
		                            </div>
		                            <div class="modal-footer">
		                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
		                                <button type="button" class="btn btn-primary">Save changes</button>
		                            </div>
		                        </div>
		                    </div>
			            </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		var self = this;
		this.on('mount', function(){
			var theimage = document.querySelector('.theimage');
			console.log(theimage);
			$(theimage).cropper({
	            aspectRatio: 1,
	            preview: ".imgpreview",
	            done: function(data) {
	                // Output the result data for cropping image.
	                console.log('test cropper');
	            }
	        });
		})

        // var $inputImage = $("#inputImage");
        // if (window.FileReader) {
        //     $inputImage.change(function() {
        //         var fileReader = new FileReader(),
        //                 files = this.files,
        //                 file;

        //         if (!files.length) {
        //             return;
        //         }

        //         file = files[0];

        //         if (/^image\/\w+$/.test(file.type)) {
        //             fileReader.readAsDataURL(file);
        //             fileReader.onload = function () {
        //                 $inputImage.val("");
        //                 $image.cropper("reset", true).cropper("replace", this.result);
        //             };
        //         } else {
        //             showMessage("Please choose an image file.");
        //         }
        //     });
        // } else {
        //     $inputImage.addClass("hide");
        // }
	</script>
    
	<style>
		/* Ensure the size of the image fit the container perfectly */
		.imgpreview {
			overflow: hidden;
			height: 192px;
			width: 192px;
		}

		img{
			display: block;
			margin-left: auto;
			margin-right: auto;
		}

	</style>
</infoprofileeditpicts>