<infoprofiletag>
    <div>
        <div class="ibox-content no-padding border-left-right" style="position: relative;">
            <img alt="image" class="img-fluid" src="{opts.base_url}/public/img/covers/default.jpg">
            <div style="position: absolute; right: 5%; bottom:5%; display: inline-block;">
                <button class="btn btn-default btn-circle btn-lg editpicts" type="button" title="Editer la couverture et la photo de profil"><i class="fa fa-picture-o"></i></button>
                <button class="btn btn-default btn-circle btn-lg editinfos" type="button" title="Editer les infos du profil"><i class="fa fa-edit"></i></button>
            </div>
        </div>
        <div class="ibox-content profile-content">
            <infoprofileshowinfos  show={editform === false && editpicts === false} 
            base_url="{opts.base_url}"
            email="{opts.email}"
            username = "{opts.username}"
            name= "{opts.name}"
            address= "{opts.address}"
            biographie= "{opts.biographie}"
            residence= "{opts.residence}"
            mobiles= "{opts.mobiles}"
            type_compte= "{opts.type_compte}"
            pays_exercice= "{opts.pays_exercice}"
            metiers= "{opts.metiers}"
            ></infoprofileshowinfos>

            <infoprofileformeditinfos show={editform === true} 
            base_url="{opts.base_url}"
            userid="{opts.userid}" 
            username = {opts.username} 
            email = {opts.email} 
            countries={opts.countries}
            secteurarray={opts.secteurarray}
            name= "{opts.name}"
            address= "{opts.address}"
            biographie= "{opts.biographie}"
            residence= "{opts.residence}"
            mobiles= "{opts.mobiles}"
            type_compte= "{opts.type_compte}"
            pays_exercice= "{opts.pays_exercice}"
            metiers= "{opts.metiers}"
            ></infoprofileformeditinfos>
            <infoprofileeditpicts show={editpicts === true} base_url="{opts.base_url}"></infoprofileeditpicts>
        </div>
    </div>

	<script>
		this.editform = false;
        this.editpicts = false;
        
		var self = this;

		this.on('mount', function(){
			document.querySelector('.editinfos').addEventListener('click', function(){
				self.editform = !self.editform;
                self.update();
			});

            document.querySelector('.editpicts').addEventListener('click', function(){
                self.editpicts = !self.editpicts;
                self.update();
            });
		});

	</script>
</infoprofiletag>