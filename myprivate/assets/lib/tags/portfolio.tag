<portfoliotag>
	<div class="feed-activity-list">
		<div class="">
			<form method="POST" class="addexperience">
				<div class="form-group  row"><label class="col-sm-2 col-form-label">Libellé</label>
                    <div class="col-sm-10"><input type="text" class="form-control" placeholder="Formuez le libellé de l'expérience" ref="libelle" required=""></div>
                </div>
                <div class="form-group  row"><label class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10"><input type="text" class="form-control dateexperience" placeholder="Sélectionnez la date" ref="date" required=""></div>
                </div>
                <div class="form-group  row">
                	<label class="col-sm-2 col-form-label" >
                		Description
						<br>
						<span class="text-warning">[- <span class="remaining">{remaining} </span> caractères]</span>
                	</label>
                    <div class="col-sm-10">
                    	<textarea name="" id="" rows="3" class="form-control" placeholder="Donne la description de votre réalisation ou de l'expérience sollicitée..." ref="description" required="" maxlength="450"></textarea>
                    </div>
                </div>
                <div class="text-center">
                	<button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Ajouter une expérience</button>
                	<button class="btn" type="reset">Annuler</button>
                </div>
			</form>

			<div class="hr-line-dashed"></div>

			<div class="text-center nodata" if={portfolioelts.length === 0}>
				<br>
				<h2 style="width:500px; margin: auto">Il n'y a actuellement aucune données disponible veuillez remplir de formulaire ci-dessus pour ajouter une expérience</h2>
				<img src="{opts.base_url}public/img/box.svg" alt="empty box" height="400">
				<br>
			</div>
		</div>
		<div class="portfoliolist" if={portfolioelts.length != 0}>
			<div class="feed-element animate fadeIn" each = "{portfoliotelt, i in portfolioelts}">
		        <a href="#" class="float-left">
		            <img alt="image" class="rounded-circle" src="{opts.base_url}public/templates/01/img/a1.jpg">
		        </a>
		        <div class="media-body ">
                    <small class="float-right tempsecoule">{showtempsecoule(portfoliotelt.date_creation)}</small>
                    <strong>{portfoliotelt.libelle}</strong> <br>
                    <small class="text-muted">{portfoliotelt.date}</small>
                    <div class="well">
                        {portfoliotelt.description}
                    </div>
                    <div class="float-right">
                        <a href="javascript:;" class="btn btn-xs btn-white" onclick="{editexperience}"><i class="fa fa-edit"></i> Edit </a>
                        <a href="javascript:;" class="btn btn-xs btn-info" onclick="{}"><i class="fa fa-file-picture-o"></i> Images</a>
                        <a href="javascript:;" class="btn btn-xs btn-info" onclick="{}"><i class="fa fa-link"></i> Links</a>
                        <a href="javascript:;" class="btn btn-xs btn-danger" onclick="{deleteexperience}"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                </div>
		    </div>
		</div>
	</div>
	
	<script>
		this.portfolioelts = [];
		this.remaining = 450;
		var self = this;

		this.showtempsecoule = function(temps){
			return jQuery.timeago(temps);
		};

		this.deleteexperience = function(){
		    swal({
		        title: "Etes vous sûr?",
		        text: "Cette action est irreversible, vous perdrez cette expérience!",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes, delete it!",
		        closeOnConfirm: false
		    }, function () {
		        swal("Deleted!", "Expérience supprimée avec succès.", "success");
				self.portfolioelts.splice(self.i, 1);
				console.log(this.portfolioelts);
				self.update();
		    });
		};

		this.on('mount', function(){
			const formAdd = document.querySelector('.addexperience');

			formAdd.addEventListener('submit', function(e){
				e.preventDefault();
				let obj = {
					libelle: self.refs.libelle.value,
					date: self.refs.date.value,
					description: self.refs.description.value,
					date_creation: new Date().getTime()
				};
				self.portfolioelts.unshift(obj);
				self.refs.libelle.value = "";
				self.refs.date.value = "";
				self.refs.description.value = "";
				self.remaining = self.refs.description.maxLength;
				document.querySelector('.remaining').textContent = self.remaining;
				self.update();
				console.log(self.portfolioelts);
			});


			self.refs.description.addEventListener('input', function(){
				self.remaining = self.refs.description.maxLength - self.refs.description.value.length*1;
				document.querySelector('.remaining').textContent = self.remaining;
			})

			$('.dateexperience').datepicker({
		        startView: 1,
		        todayBtn: "linked",
		        keyboardNavigation: false,
		        forceParse: false,
		        autoclose: true,
		        format: "dd/mm/yyyy"
		    });
		});
	</script>
</portfoliotag>