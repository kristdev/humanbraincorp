<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_expertises extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('expertise');
		$crud->set_subject('expertise');
		$crud->set_field_upload('baniere','assets/uploads/expertise/baniere/');
		$crud->callback_add_field('libelle', function(){
        	return '<input type="text" maxlength="50" value="" class="form-control input_libelle" name="libelle">';
        });
        $crud->callback_edit_field('libelle', function($value){
        	return '<input type="text" maxlength="50" value="'.$value.'" class="form-control input_libelle" name="libelle">';
        });
		$crud->callback_add_field('slug', function(){
			return '<input type="text" maxlength="50" class="form-control input_slug" name="slug">';
		});
		$crud->callback_edit_field('slug', function($value){
			return '<input type="text" maxlength="50" value="'.$value.'" class="form-control input_slug" name="slug">';
		});

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

}

?>