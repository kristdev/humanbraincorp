<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Artconnectafrica | 404 Error</title>

    <link href="<?php echo base_url(); ?>public/templates/01/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/templates/01/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>public/templates/01/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/templates/01/css/style.css" rel="stylesheet">

    <style>
        .btn-primary, .btn-primary:hover, .btn-primary:focus, .btn-primary.focus {
            background-color: #845013;
            border-color: #845013;
            color: #FFFFFF;
        }
    </style>

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Page Not Found</h3>

        <div class="error-desc">
            Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the "Go back" button.
            <button class="btn btn-primary" onclick="history.back();">Go back</button>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>public/templates/01/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/templates/01/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>public/templates/01/js/bootstrap.js"></script>

</body>

</html>
