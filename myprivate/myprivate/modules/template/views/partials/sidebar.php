<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element text-center">
                <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>assets/img/profile-user.svg" height="48"/>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="block m-t-xs font-bold"><?php echo $this->ion_auth->user()->row()->username; ?></span>
                    <span class="text-muted text-xs block"><b class="caret"></b></span>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <!-- <li><a class="dropdown-item" href="profile.html">Profile</a></li> -->
                    <!-- <li><a class="dropdown-item" href="contacts.html">relations</a></li>
                    <li><a class="dropdown-item" href="mailbox.html">settings</a></li> -->
                    <li class="dropdown-divider"></li>
                    <li><a class="dropdown-item logout2" href="javascript:;">Logout</a></li>
                </ul>
            </div>
            <div class="logo-element">
                IN+
            </div>
        </li>
        <!-- <li class="active">
            <a href="<?php echo base_url(); ?>dashboard" up-target=".contenthere" up-instant up-preload><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
        </li>
        <li>
            <a href="mailbox.html"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Mon journal</span></a>
        </li>
        <li>
            <a href="mailbox.html"><i class="fa fa-briefcase"></i> <span class="nav-label">Opportunités</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="graph_flot.html">Offres</a></li>
                <li><a href="graph_chartist.html">Projets</a></li>
            </ul>
        </li>
        <li>
            <a href="mailbox.html"><i class="fa fa-address-book"></i> <span class="nav-label">Relations </span></a>
        </li>
        <li>
            <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Mailbox </span><span class="label label-warning float-right">24</span></a>
        </li>
        <li>
            <a href="mailbox.html"><i class="fa fa-bell"></i> <span class="nav-label">Notifications </span><span class="label label-info float-right">50</span></a>
        </li> -->
        <li class="menu-sidebar active">
            <a href="<?php echo base_url(); ?>dashboard" up-target=".contenthere" up-transition="cross-fade">
                <i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>users">
                <i class="fa fa-users"></i> <span class="nav-label">Utilisateurs</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>couverture">
                <i class="fa fa-slideshare"></i> <span class="nav-label">Couverture</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>expertises" up-target=".contenthere" up-transition="cross-fade">
                <i class="fa fa-briefcase"></i> <span class="nav-label">Expertises</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>candidatures" up-target=".contenthere" up-transition="cross-fade">
                <i class="fa fa-briefcase"></i> <span class="nav-label">Candidatures</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>offres" up-target=".contenthere" up-transition="cross-fade">
                <i class="fa fa-bullhorn"></i> <span class="nav-label">Offres d'emplois</span>
            </a>
        </li>
        <li class="menu-sidebar">
            <a href="<?php echo base_url(); ?>blog">
                <i class="fa fa-book"></i> <span class="nav-label">Blog</span>
            </a>
            <!-- <ul class="nav nav-second-level">
                <li><a href="typography.html" up-target=".contenthere" up-transition="cross-fade">Catégories</a></li>
                <li><a href="typography.html" up-target=".contenthere" up-transition="cross-fade">Sous Catégories</a></li>
                <li><a href="typography.html" up-target=".contenthere" up-transition="cross-fade">Articles</a></li>
            </ul> -->
        </li>
    </ul>
</div>