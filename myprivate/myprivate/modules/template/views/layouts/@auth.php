<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>HBC admin | <?php echo $metatitle ?? null; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url().'assets/templates/01/' ?>hbfavicon.ico" type="image/x-icon">

    <link href="<?php echo base_url().'assets/templates/01/' ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/templates/01/' ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url().'assets/templates/01/' ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/templates/01/' ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/' ?>vegas/vegas.min.css" rel="stylesheet">

    <style>
        body.vegas-container{
            position: relative;
        }
        .bgc-white {
            background-color: rgba(255,255,255,0.9);
        }

        .bgc-transparent{
            background-color: transparent;
        }

        .loginColumns {
            max-width: 800px;
            margin: auto;
            padding: 20px 20px 20px 20px;
            position: absolute;
            top:0;
            left:0;
            bottom: 0;
            right:0;
        }

        .btn-primary, .btn-primary:hover, .btn-primary:focus, .btn-primary.focus {
            background-color: #DA7A24;
            border-color: #DA7A24;
            color: #FFFFFF;
        }

        .childvert{
            display: flex;
            align-items: center;
        }

        .transition-fade {
          transition: 0.4s;
          opacity: 1;
        }

        html.is-animating .transition-fade {
          opacity: 0;
        }

    </style>

</head>

<body class="white-bg">

    <div class="loginColumns animated fadeInDown childvert">
        <div class="row bgc-white">
            <div class="col-md-6 childvert" style="background-color: white; padding-top: 10px; padding-bottom: 10px;">
               <img src="<?php echo base_url(); ?>assets/templates/01/img/logohbc.svg" alt="HBC Logo" class="img-fluid" width="350">
            </div>
            <div class="col-md-6">
                <div class="ibox-content bgc-transparent transition-fade" id="swup">
                    <?php 
                        if (isset($view) and isset($module)) {
                            $this->load->view($module.'/'.$view);
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <script src="<?php echo base_url().'assets/templates/01/' ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url().'assets/' ?>vegas/vegas.min.js"></script>
    <script src="<?php echo base_url().'assets/' ?>swup/swup.min.js"></script>
    <!-- <script src="<?php echo base_url().'assets/' ?>myjs.js"></script> -->

    <script>
        function mainjs(){
            $("body").vegas({
                slides: [
                    { src:"<?php echo base_url(); ?>assets/templates/01/img/bg03.jpg" }
                ],
                overlay: true
            });
        }

        mainjs();

    </script>
</body>

</html>
