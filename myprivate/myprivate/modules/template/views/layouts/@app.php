<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>HBC | <?php echo $metatitle ?? null; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url().'assets/templates/01/' ?>hbfavicon.ico" type="image/x-icon">

    <link href="<?php echo base_url(); ?>assets/templates/01/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/templates/01/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url(); ?>assets/templates/01/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <!-- Choosen -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Date Picker -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    
    <!-- Cropper -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/plugins/cropper/cropper.min.css" rel="stylesheet">

    <!-- Unpoly -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unpoly/unpoly.min.css">
    
    <!-- animate css -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/animate.css" rel="stylesheet">

    <!-- Flags icons css -->
    <link href="<?php echo base_url(); ?>assets/flags/flags.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="<?php echo base_url(); ?>assets/templates/01/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar -->
            <?php $this->load->view('partials/sidebar'); ?>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <!-- header -->
                <?php $this->load->view("partials/header"); ?>
            </div>
            <!-- <div id="allallertes" class="row">
                <div class="col-md-12">
                    Alerte profile
                    <div class="alert alert-danger alert-dismissable m-t-md">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Afin de profiter de toute la puissance du réseau, complètez votre profil. <a class="alert-link" href="#">Cliquez ici!</a>.
                    </div>
                </div>
            </div> -->
            <div class="contenthere">  
                <!-- pages loads here -->
                <?php $this->load->view($page = (isset($module) && isset($view)) ? $module.'/'.$view : null); ?>

                <!-- Modal Welcome -->
                <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 999999">
                    <div class="modal-dialog modal-dialog-center">
                    <div class="modal-content animated bounceInRight">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <!-- <i class="fa fa-laptop modal-icon"></i> -->
                                <div class="text-center">
                                    <img src="<?php echo base_url(); ?>assets/templates/01/img/comingsoon.svg  ?>" alt="Welcome" style="height: 300px;">
                                </div>
                                <h4 class="modal-title">Bienvenue sur Art Connect Africa</h4>
                                <small class="font-bold">Plateforme de coopération et d'échanges culturels en Afrique (V.0.0.1)</small>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <strong><?php echo $this->ion_auth->user()->row()->username; ?></strong> 
                                    Nous sommes content de vous compter parmi nous et espérons que vous aurez ou alors que vous faites une expérience des plus agréables sur notre plateforme. Nos équipes techniques sont en train de travailler dur, mettant sur pied de nouvelles fonctionnalités pour votre satisfaction et votre épanouissement professionnel.
                                </p>
                                <small class="font-bold">
                                    <p><strong>A venir (V.0.0.2):</strong></p>
                                    <ul>
                                        <li><strong>Système de notifications</strong> (Système de notifications automatiques)</li>
                                        <li><strong>Système d'amitiés et de suivis</strong> (Possibilité d'établir des relations d'amitié et suivre des membres)</li>
                                        <li><strong>Implémentation d'un Dashboard</strong> (Le tableau de board est un module recapitulatif et statistique des différentes fonctionnalités mises sur pied)</li>
                                    </ul>
                                </small>
                                <small class="font-bold">
                                    <p><strong>Déjà implémenté (V.0.0.1)</strong></p>
                                    <ul>
                                        <li><strong>Gestion de profils </strong> (L'utilisateur peut ici remplir et modifier son profil)</li>
                                        <li><strong>Gestion d'expériences </strong>(L'utilisateur peut ajouter, modifier ou supprimer des expériences dans son portfolio, et y attacher des images, des vidéos, et divers liens)</li>
                                        <li><strong>Gestion de tickets </strong> (L'utilisateur peut créer des tickets relatifs aux problèmes qu'il rencontre dans la plateforme et attendre des retours des administrateurs)</li>
                                    </ul>
                                </small>
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-white" data-dismiss="modal">Close</button> -->
                                <button type="button" class="btn btn-primary" data-dismiss="modal">J'ai compris</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Pacejs -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/pace/pace.min.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- ChartJS-->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/chartJs/Chart.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Toastr -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/toastr/toastr.min.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Data picker -->
   <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- timeago -->
   <script src="<?php echo base_url(); ?>assets/assets/timeago/timeago.js"></script>

    <!-- Unpoly -->
    <script src="<?php echo base_url(); ?>assets/unpoly/unpoly.min.js"></script>

    <!-- Velocity.js -->
    <script src="<?php echo base_url(); ?>assets/velocity/velocity.min.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo base_url(); ?>assets/templates/01/js/plugins/cropper/cropper.min.js"></script>

    <!--Mediawrapper-->
    <script src="<?php echo base_url(); ?>assets/jquery.mediaWrapper.js"></script>

    <!-- Riotjs -->
    <script src="<?php echo base_url(); ?>assets/riot/riot-3.13.2.min.js"></script>

    <!-- lib -->
    <script src="<?php echo base_url(); ?>assets/lib/tagscompiled/alltags.js"></script>

    <!-- Custom and plugin javascript -->
    <!-- <script src="<?php echo base_url(); ?>assets/templates/01/js/demo/peity-demo.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>assets/templates/01/js/demo/sparkline-demo.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/tpl.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tpl_unpoly.js"></script>
    <script src="<?php echo base_url();?>assets/lib/main.js"></script>

    <script>
        $(document).ready(function() {
            up.compiler('.contenthere', function(e){
                // Iframe du grocery crud
                $('iframe').mediaWrapper({
                    intrinsic:  true
                });
                // Etat actif du menu
                var currentlocation = location.href;
                var menuitemarray = document.querySelectorAll('.menu-sidebar');
                for(var i=0; i<menuitemarray.length; i++){
                    menuitemarray[i].className = "menu-sidebar";
                    if(menuitemarray[i].firstElementChild.href === currentlocation){
                        menuitemarray[i].className = "menu-sidebar active";
                    }
                }
            })
        });
    </script>
</body>
</html>
