<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->helper('url');
	}

	public function index(){}

	public function auth($data=null){
		$this->load->view('layouts/@auth', $data);
	}
	/**
	 * Gère le temlate dashboard du RS
	 * @param  array $data module, vue et élément necessaires aux divers affichages
	 * @return void       Affiche la page demandée
	 */
	public function app($data=null){
		$this->load->view('layouts/@app', $data);
	}
	/**
	 * Affiche la page 404 Error
	 * @return void la page 404
	 */
	public function error404(){
		$this->load->view('_404');
	}

	public function error500(){
		$this->load->view('_500');
	}

}

?>