<div class="pageloading">
	
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
			<h2>Le blog</h2>
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="#">Root</a>
				</li>
				<li class="breadcrumb-item active">
					<strong>blog</strong>
				</li>
			</ol>
		</div>
		<div class="col-sm-8">
			<!-- <div class="title-action">
				<a href="" class="btn btn-primary">This is action area</a>
			</div> -->
		</div>
	</div>

	<div class="wrapper wrapper-content">
		<div class="col-lg-12">
			<div class="ibox ">
				<div class="ibox-title">
					<h5>
						Gestion du blog
						<!-- <small class="m-l-sm">This is custom panel</small>-->
					</h5> 
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-wrench"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="#" class="dropdown-item">Config option 1</a>
							</li>
							<li>
								<a href="#" class="dropdown-item">Config option 2</a>
							</li>
						</ul>
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<div class="animated fadeInRightBig">
						<div class="tabs-container">
	                        <ul class="nav nav-tabs" role="tablist">
	                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"> Catégories</a></li>
	                            <li><a class="nav-link" data-toggle="tab" href="#tab-2">Articles</a></li>
	                        </ul>
	                        <div class="tab-content">
	                            <div role="tabpanel" id="tab-1" class="tab-pane active">
	                                <div class="panel-body">
	                                	<iframe src="<?php echo base_url(); ?>gc_categoriesblog" frameborder="0"></iframe>
	                                </div>
	                            </div>
	                            <div role="tabpanel" id="tab-2" class="tab-pane">
	                                <div class="panel-body">
	                                	<iframe src="<?php echo base_url(); ?>gc_articlesblog" frameborder="0"></iframe> 
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
