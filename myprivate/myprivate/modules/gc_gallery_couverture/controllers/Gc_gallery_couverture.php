<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_gallery_couverture extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('gallery_couverture');
		$crud->set_subject('image de couverture');
		$crud->set_field_upload('path','assets/uploads/couverture');
		$crud->set_relation('couverture_id','couverture','libelle');
		$crud->display_as('path','image');

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

}

?>