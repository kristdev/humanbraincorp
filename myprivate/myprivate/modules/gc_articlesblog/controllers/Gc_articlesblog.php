<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_articlesblog extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
		$this->load->helper('date');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('post_blog');
		$crud->set_subject('article');
        // $crud->unset_edit_fields('created');
        // $crud->unset_add_fields('created');
        $crud->unset_edit_fields('created');
		$crud->set_field_upload('baniere','assets/uploads/blog/baniere');
		$crud->set_relation('categories_blog_id','categories_blog','libelle');
        $crud->field_type('created', 'hidden', now());
        $crud->field_type('updated', 'hidden', now());


        $crud->callback_column('created',array($this,'formatdate'));
        $crud->callback_column('updated',array($this,'formatdate'));

		$crud->callback_add_field('titre', function(){
        	return '<input type="text" maxlength="50" value="" class="form-control input_titre" name="titre">';
        });
        $crud->callback_add_field('slug', function(){
        	return '<input type="text" maxlength="50" value="" class="form-control input_slug" name="slug">';
        });
		$crud->callback_read_field('created', function ($value, $primary_key) {
            return now();
        });
        $crud->callback_read_field('updated', function ($value, $primary_key) {
            return now();
        });

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

    public function formatdate($value){
        return unix_to_human($value, TRUE, 'eu');
    }

}

?>