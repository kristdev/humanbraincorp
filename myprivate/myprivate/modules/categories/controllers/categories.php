<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
	}
	
	public function index()
	{
		$data['header_title'] = "Regroupon | catégories des produits";
		$data['module'] = "categories";
		$data['view'] = "categories";
		echo Modules::run('template/app', $data);
	}

}

?>