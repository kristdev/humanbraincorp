<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_categorie_produit extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
		$this->config->set_item('grocery_crud_file_upload_allow_file_types', 'jpeg|jpg');
	}
	
	public function index(){
		$crud = new grocery_CRUD();
		$crud->set_table('categorie');
		$crud->set_subject('catégorie');
		$crud->required_fields('libelle');
		$crud->set_field_upload('image', 'assets/uploads/files/affiches_categories');
		$crud->callback_before_upload(array($this,'checkaffiche'));
		$crud->callback_after_upload(array($this,'resizeaffiche'));
		$output = $crud->render();
		$this->load->view('output',$output);
	}
	
	//	Vérifie l'affiche catégorie avavnt upload
	public function checkaffiche($files,$field){
		if (json_encode($files[$field->encrypted_field_name]['size']) > 6000000) {
		 	# code...
		 	return "Fichier trop lourd";
		 }
	}

	public function resizeaffiche($uploader_response,$field_info, $files_to_upload){
		$this->load->library('image_moo');
		 
	    //Is only one file uploaded so it ok to use it with $uploader_response[0].
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
	 
	    $this->image_moo->load($file_uploaded)
	    		->resize_crop(800,800)
	    		->save($file_uploaded,true);
	 
	    return true;
	}

}

?>