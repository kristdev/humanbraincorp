<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php
		foreach ($css_files as $file)
			: ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
		<?php endforeach; ?>
	</head>
	<body>
		<div>
			<?php echo $output; ?>
		</div>
		<?php
		foreach ($js_files as $file)
			: ?>
		<script src="<?php echo $file; ?>"></script>
		<?php endforeach; ?>
		<script>
			function slugify(text) {
			  return text
			    .toString()                     // Cast to string
			    .toLowerCase()                  // Convert the string to lowercase letters
			    .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
			    .trim()                         // Remove whitespace from both sides of a string
			    .replace(/\s+/g, '-')           // Replace spaces with -
			    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			    .replace(/\-\-+/g, '-');        // Replace multiple - with single -
			}

			const inputtitre = document.querySelector('.input_libelle');

			inputtitre.addEventListener('input', updateValue);

			function updateValue() {
			  document.querySelector('.input_slug').value = slugify(inputtitre.value);
			}
		</script>
	</body>
</html>