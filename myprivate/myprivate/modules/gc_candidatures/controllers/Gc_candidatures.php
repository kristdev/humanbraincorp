<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_candidatures extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
		$this->load->helper('date');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('postulats');
		$crud->set_subject('candidature');
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();
        $crud->set_relation('offre_id','offres','{slug} / {societe}');
        // $crud->set_relation('cv_id','cv','<a href="">{file}</a>');
        $crud->set_relation('candidat_id','users','{username} / {first_name} {last_name} / {email}');
        $crud
        	->display_as('offre_id','Offre')
        	->display_as('cv_id','CV file')
        	->display_as('candidat_id','Candidat');
        $crud->callback_column('cv_id',array($this,'_callback_webpage_url'));
        $crud->callback_read_field('cv_id',array($this,'_callback_webpage_url'));

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

    public function formatdate($value){
        return unix_to_human($value, TRUE, 'eu');
    }

    public function _callback_webpage_url($value, $row){
    	$this->db->where('id', $value);
    	$query = $this->db->get('cv');
    	$queryrow = $query->row();
    	$filename = $queryrow->file;
	  return "<a href='".base_url()."assets/uploads/cv/".$filename."'>$filename</a>";
	}

}

?>