<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offres extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
	}

	public function index(){
		if(!$this->ion_auth->logged_in()){
			redirect(base_url());
		}else{
			$data['metatitle'] = "Gestion des offres";
			$data['module'] = "offres";
			$data['view'] = "offrespage";
			echo Modules::run('template/app', $data);
		}
	}

}

?>