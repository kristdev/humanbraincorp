<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->library('encryption');
		$this->load->model('mdl_auth', 'mdlauth');
		$this->load->helper('string');
	}

	/**
	 * Fonction par défaut de la classe
	 * @return void retourne une vue
	 */
	public function index(){
		if (!$this->ion_auth->logged_in()){
	      $this->login();
	    }else{
	    	$user = $this->ion_auth->user()->row();
	    	$id = $user->id;
	    	$username = $user->username;
	    	$email = $user->email;
	    	$createdon = $user->created_on;
	    	if(Modules::run('user/cb_checkuser', $id) == false){
	    		Modules::run('user/cb_createuser', $id,$username,$email,$createdon);
	    	}
	    	//echo Modules::run('dashboard');
	    	redirect(base_url().'dashboard');
	    }
	}



	/**
	 *  ============= Fonction de connexion et déconnexion ==========
	 */

	/**
	 * Fonction d'authentification
	 * @return void Retourne la vue d'authentification
	 */
	public function login(){
		if(!$this->ion_auth->logged_in()){
			$input = $this->input->post();
			if($input){
				$identity = $this->security->xss_clean($input['email']);
				$password = $this->security->xss_clean($input['password']);
				$remember = $input['remember'] ?? false;

				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

				if($this->form_validation->run() && $this->ion_auth->login($identity, $password, $remember) == true && $this->ion_auth->is_max_login_attempts_exceeded($identity) == false && $this->ion_auth->is_admin()== true ){
					redirect(base_url());
				}else{
					$errors = $this->ion_auth->errors();
      				$data['errors'] = $errors;
				}
			}
			$data['metatitle'] = "Connexion";
			$data['module'] = "auth";
			$data['view'] = "login";
			echo Modules::run('template/auth', $data);
		}else{
			redirect(base_url());		
		}
	}
	/**
	 * Deconnexion utilisateur
	 * @return void Renvoie sur la page de connexion
	 */
	public function logout(){
		$this->ion_auth->logout();
		redirect(base_url());	
	}



	/**
	 * =========== Fonctions d'inscription ========
	 */
	/**
	 * Inscription des membres
	 * @return void la vue inscription
	 */
	public function inscription(){
		$data['view'] = "inscription";
		if(!$this->ion_auth->logged_in()){
			$input = $this->input->post();
			if($input){
				$email = $this->security->xss_clean($input['email']);
				$username = $this->security->xss_clean($input['username']);
				$password = $this->security->xss_clean($input['password']);
				$repassword = $this->security->xss_clean($input['repassword']);

				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
				$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|is_unique[users.username]');
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
				$this->form_validation->set_rules('repassword', 'Repassword', 'required|min_length[5]|matches[password]');

				if($this->form_validation->run()){
					$id = $this->ion_auth->register($username, $password, $email);
					if($id != false){
						$data['view'] = 'inscription_validation';
						$codecreated = $this->create_activation_code($id);
						$urlactivation = 'http://myartconnectafrica.loc/auth/confirm-inscription/'.$codecreated;
						if($codecreated != false){
							$datasend = array(
								'from'			=> 		'noreply@artconnectafrica.com',
								'to'			=>		array(
									[
										'address' => $email
									]
								),
								'subject'		=> 		'Complètez votre inscription',
								'body'			=>		'<!doctype html> <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> <head> <title>Complétez votre inscription - Art Connect Africa </title> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> #outlook a{padding:0;}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table, td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}img{border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}p{display:block;margin:13px 0;}</style><!--[if mso]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml><![endif]--><!--[if lte mso 11]> <style type="text/css"> .mj-outlook-group-fix{width:100% !important;}</style><![endif]--> <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"> <style type="text/css"> @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700); </style> <style type="text/css"> @media only screen and (min-width:480px){.mj-column-per-100{width:100% !important; max-width: 100%;}.mj-column-per-50{width:50% !important; max-width: 50%;}.mj-column-per-90{width:90% !important; max-width: 90%;}}</style> <style type="text/css"> @media only screen and (max-width:480px){table.mj-full-width-mobile{width: 100% !important;}td.mj-full-width-mobile{width: auto !important;}}</style> </head> <body> <div style="" ><!--[if mso | IE]> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> </tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-top:#9f170b 5px solid;direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-left:0px;word-break:break-word;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;" > <tbody> <tr> <td style="width:600px;"> <img height="auto" src="https://mails.artconnectafrica.com/inscription/mailbanneraca.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600"/> </td></tr></tbody> </table> </td></tr></table> </div><!--[if mso | IE]> </td><td class="" style="vertical-align:top;width:300px;" ><![endif]--> <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" ><strong>Complétez votre inscription!</strong></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="word-wrap-outlook" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix word-wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left:#d65c13 10px solid;vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Une demande d\'insciption en cours a été ouverte ppour le compte '.$username.' par '.$email.'  ,</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Il vou reste une seule étape pour dorénavant faire partir de la grande famille Art Connect Africa.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Veuillez recopier le lien ci-dessous et le coller dans votre navigateur préféré ou clicquez sur le bouton en dessous pour complèter votre inscription.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" ><a href="'.$urlactivation.'">'.$urlactivation.'</a></div></td></tr><tr> <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;" > <tr> <td align="center" bgcolor="#414141" role="presentation" style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#414141;" valign="middle" > <a href="'.$urlactivation.'" style="display:inline-block;background:#414141;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;" target="_blank" > Cliquez ici! </a> </td></tr></table> </td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Cordialement!</div></td></tr><tr> <td align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;" ><br></br></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </div></body> </html>'
							);


							$datasend = http_build_query($datasend);
							$url = base_url().'api/mail/sendmailnotls';
							$ch = curl_init($url);
							curl_setopt_array($ch, [
								CURLOPT_RETURNTRANSFER			=>			true,
								CURLOPT_POST					=>			1,
								CURLOPT_POSTFIELDS				=>			$datasend
							]);
							$datacurl = curl_exec($ch);
							if($datacurl === false){
								var_dump(curl_error($ch));
							}else{
								if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200){
									$data['message_inscription'] = "<p><strong>vous devez valider votre inscription en cours!</strong> </p><p>Vous avez reçu un mail de confirmation à l'adresse fournie, veuillez vous y rendre et confirmer votre inscription. Le lien d'activation fourni a un délai de validité de 24h</p>";
								}else{
									$data['message_inscription'] = "<p><strong>Erreur sur l'inscription en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
								}
							}
							curl_close($ch);
						}else{
							$data['message_inscription'] = "<p><strong>Erreur sur l'inscription en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
						}
					}else{
						$data['message_inscription'] = "<p><strong>Erreur sur l'inscription en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
					}
				}else{
					$errors = $this->ion_auth->errors();
      				$data['errors'] = $errors;
				}
			}
			$data['metatitle'] = "Inscription";
			$data['module'] = "auth";
			echo Modules::run('template/auth', $data);
		}else{
			rediect(base_url());
		}
	}
	/**
	 * Fonction de création du code d'activation
	 * @param  int $id Identifiant de l'utilisateur
	 * @return string     Code d'activation
	 */
	public function create_activation_code($id){
		$code = $this->encryption->encrypt($id);
		$code_encoded = str_replace(array('+', '/', '='), array('!', '.', '~'), $code);
		$table = "activation_codes";
		$data = array(
			'code'				=>		$code,
			'code_encoded'		=>		$code_encoded,
			'date_creation'		=>		time(),
			'user'				=>		$id
		);
		if($this->mdlauth->insert_somedata($table,$data)){
			return $code_encoded;
		}else{
			return false;
		}
		// echo $code;
		// echo '<br/>';
		// echo $code_encoded;
	}
	/**
	 * Fonction de confirmation de l'inscription
	 * @param  string $code Code d'activation
	 * @return void       Active l'utilisateur
	 */
	public function confirm_inscription($code=null){
		if(!$this->ion_auth->logged_in()){
			if($code == null){
				$data['message_inscription'] = "Le lien d'activation donné n'est pas valide, veuillez vous assurer de ne pas avoir commis d'erreurs ou recommencez la procédure d'inscription";
			}else{
				$code_decoded = str_replace(array('!', '.', '~'), array('+', '/', '='), $code);
				$decode = $this->encryption->decrypt($code_decoded);
				$id = $decode;
				$where = array(
					'id'		=>		$id
				);
				$result = $this->mdlauth->get_allwhereresults('users',$where);
				if($result->num_rows() != 0 && $this->check_activation_code($code) === true){
				    $datasent = array(
				          'active' => 1,
				           );
				    if($this->ion_auth->update($id, $datasent) == true){
				    	$data['message_inscription'] = "Votre inscription a bien été confirmée, vous pouvez dorénavant vous connecter";
				    	$data['inscriptionsuccessclass'] = 'alert-success';
				    	$datasend = array(
							'from'			=> 		'noreply@artconnectafrica.com',
							'to'			=>		array(
								[
									'address' => $this->ion_auth->user($id)->row()->email
								]
							),
							'subject'		=> 		'Bienvenue sur notre plateforme',
							'body'			=>		'<!doctype html> <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> <head> <title>Bienvenue sur notre plateforme- Art Connect Africa </title> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> #outlook a{padding:0;}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table, td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}img{border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}p{display:block;margin:13px 0;}</style><!--[if mso]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml><![endif]--><!--[if lte mso 11]> <style type="text/css"> .mj-outlook-group-fix{width:100% !important;}</style><![endif]--> <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"> <style type="text/css"> @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700); </style> <style type="text/css"> @media only screen and (min-width:480px){.mj-column-per-100{width:100% !important; max-width: 100%;}.mj-column-per-50{width:50% !important; max-width: 50%;}.mj-column-per-90{width:90% !important; max-width: 90%;}}</style> <style type="text/css"> @media only screen and (max-width:480px){table.mj-full-width-mobile{width: 100% !important;}td.mj-full-width-mobile{width: auto !important;}}</style> </head> <body> <div style="" ><!--[if mso | IE]> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> </tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-top:#9f170b 5px solid;direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-left:0px;word-break:break-word;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;" > <tbody> <tr> <td style="width:600px;"> <img height="auto" src="https://mails.artconnectafrica.com/inscription/mailbanneraca.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600"/> </td></tr></tbody> </table> </td></tr></table> </div><!--[if mso | IE]> </td><td class="" style="vertical-align:top;width:300px;" ><![endif]--> <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" ><strong>Bienvenue sur notre plateforme</strong></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="word-wrap-outlook" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix word-wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left:#d65c13 10px solid;vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Félicitations, Vous êtes dorénavant membre de Art Connect Africa.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Pour vous connecter, suivez ce lien, <a href="http://myartconnectafrica.loc/auth/login/">http://myartconnectafrica.loc/auth/login/</a>. Vous devrez une fois connecté remplir les informations de votre profil afin de mieux exploiter les opportunités du réseau...</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Art Connect Africa est une plateforme de coopération et d\'échanges en aAfrique visant ...</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Cordialement!</div></td></tr><tr> <td align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;" ><br></br></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </div></body> </html> '
						);
						$datasend = http_build_query($datasend);
						$url = base_url().'api/mail/sendmailnotls';
						$ch = curl_init($url);
						curl_setopt_array($ch, [
							CURLOPT_RETURNTRANSFER			=>			true,
							CURLOPT_POST					=>			1,
							CURLOPT_POSTFIELDS				=>			$datasend
						]);
						$datacurl = curl_exec($ch);
						if($datacurl === false){
							var_dump(curl_error($ch));
						}else{
							if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200){
								
							}else{
								
							}
						}
						curl_close($ch);
				    }else{
				    	$data['message_inscription'] = "Une erreur est survenue, peut-être que le lien d'activation donné n'est pas valide, veuillez vous assurer de ne pas avoir commis d'erreurs ou recommencez la procédure d'inscription";
				    }
				}else{
					$data['message_inscription'] = "Une erreur est survenue, il semble que le lien d'activation fourni n'existe pas ou est expiré, veuillez vous assurer de ne pas avoir commis d'erreurs ou recommencez la procédure d'inscription";
				}
			}
			$data['metatitle'] = "Confirmation Inscription";
			$data['module'] = "auth";
			$data['view'] = "inscription_confirmation";
			echo Modules::run('template/auth', $data);
		}else{
			redirect(base_url());
		}
	}
	/**
	 * Vérifie le code d'activation fournie
	 * @param  string $code code d'activation
	 * @return bool       renvoie un booléen et supprime la demande d'inscription en cas de délais dépassé
	 */
	public function check_activation_code($code=null){
		if($code === null){
			return false;
		}else{
			$table = 'activation_codes';
			$field = array('date_creation', 'user');
			$where = array(
				'code_encoded'	=>	$code,
			);
			$resultquery = $this->mdlauth->get_somewhereresults($table,$field,$where);
			if($resultquery->num_rows() == 0){
				return false;
			}else{
				$row = $resultquery->row();
				$date_creation = $row->date_creation;
				$userrow= $this->ion_auth->user($row->user)->row();
				$activestate = $userrow->active;
				if((time() - $date_creation > 60*60*24) && $activestate == 0) {
					$this->ion_auth->delete_user($row->user);
					return false;
				}else{
					return true;
				}
			}
		}
	}



	/**
	 * ========== Fonctions de reinitialisation du MP ===========
	 */
	/**
	 * Fonction de modification du mot de asse
	 * @return void retourne la vue de demande du rset
	 */
	public function reset_password(){
		if(!$this->ion_auth->logged_in()){
			$input = $this->input->post();
			if($input){
				$identity = $this->security->xss_clean($input['email']);
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

				if($this->form_validation->run() && $this->ion_auth->email_check($identity)){
					$id = $this->get_id_from_email($identity);
					$code_encoded = $this->create_reset_code($id);

					if($code_encoded != false){
						$urlactivation = 'http://myartconnectafrica.loc/auth/confirm-reset-password/'.$code_encoded;
						$datasend = array(
							'from'			=> 		'noreply@artconnectafrica.com',
							'to'			=>		array(
								[
									'address' => $identity
								]
							),
							'subject'		=> 		'Reinitialisation de votre mot de passe',
							'body'			=>		'<!doctype html> <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> <head> <title>Reinitialisation de votre mot de passe- Art Connect Africa </title> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> #outlook a{padding:0;}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table, td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}img{border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}p{display:block;margin:13px 0;}</style><!--[if mso]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml><![endif]--><!--[if lte mso 11]> <style type="text/css"> .mj-outlook-group-fix{width:100% !important;}</style><![endif]--> <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"> <style type="text/css"> @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700); </style> <style type="text/css"> @media only screen and (min-width:480px){.mj-column-per-100{width:100% !important; max-width: 100%;}.mj-column-per-50{width:50% !important; max-width: 50%;}.mj-column-per-90{width:90% !important; max-width: 90%;}}</style> <style type="text/css"> @media only screen and (max-width:480px){table.mj-full-width-mobile{width: 100% !important;}td.mj-full-width-mobile{width: auto !important;}}</style> </head> <body> <div style="" ><!--[if mso | IE]> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> </tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-top:#9f170b 5px solid;direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-left:0px;word-break:break-word;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;" > <tbody> <tr> <td style="width:600px;"> <img height="auto" src="https://mails.artconnectafrica.com/inscription/mailbanneraca.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600"/> </td></tr></tbody> </table> </td></tr></table> </div><!--[if mso | IE]> </td><td class="" style="vertical-align:top;width:300px;" ><![endif]--> <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" ><strong>Reinitialisation de votre mot de passe</strong></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="word-wrap-outlook" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix word-wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left:#d65c13 10px solid;vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Vous avez solliciter une reinitialisation de votre mot de passe pour le compte '.$identity.'.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Si c\'est bien vous qui avez solliciter cette action, vous pouvez cliquer sur le lien suivant <a href="'.$urlactivation.'">'.$urlactivation.'</a>. Une fois que vous aurez cliqué sur ce lien, vous recevrez un mail avec un nouveau mot de passe aléatoire de 7 caractères qui vous permettra de vous reconnecter à nouveau.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Attention, ce lien de renitialisation a une durée de validité de 24H, passé ce délai, si vous n\'avez pas encore généré un mot de passe, vous devrez recommencer la procédure d\'initialisation.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Cordialement!</div></td></tr><tr> <td align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;" ><br></br></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </div></body> </html> '
						);


						$datasend = http_build_query($datasend);
						$url = base_url().'api/mail/sendmailnotls';
						$ch = curl_init($url);
						curl_setopt_array($ch, [
							CURLOPT_RETURNTRANSFER			=>			true,
							CURLOPT_POST					=>			1,
							CURLOPT_POSTFIELDS				=>			$datasend
						]);
						$datacurl = curl_exec($ch);
						if($datacurl === false){
							// var_dump(curl_error($ch));
							$data['message_inscription'] = "<p><strong>Erreur sur l'inscription en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
						}else{
							if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200){
								$data['message_reset_password'] = "<p><strong>vous avez reçu un mail avec le lien de reinitialisation!</strong> </p><p>Veuillez vous rendre à l'adresse ementionnée pour activer la renitialisation de votre mot de passe.";
								$data['class_success'] = 'alert-success';
							}else{
								$data['message_reset_password'] = "<p><strong>Erreur sur l'inscription en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
							}
						}
						curl_close($ch);
					}else{
						$data['message_reset_password'] = "<p><strong>Erreur sur la procédure de reinitialisation de mot de passe en cours!</strong> </p><p>Veuillez recommencer la procédure!.</p>";
					}
				}else{
					$data['message_reset_password'] = "Cette adresse mail n'existe pas ou est incorrecte, veuillez rependre la procédure!";
				}
			}
			$data['metatitle'] = "Password Reset";
			$data['module'] = "auth";
			$data['view'] = "reset_password";
			echo Modules::run('template/auth', $data);
		}else{
			redirect(base_url());
		}
	}
	/**
	 * Fonction de création du code de reinitialisation du MP
	 * @param  int $id Identifiant utilsateur
	 * @return boo/string     Renvoie false si Erreur et la chaine de reinitialisation si succès
	 */
	public function create_reset_code($id){
		$code = $this->encryption->encrypt($id);
		$code_encoded = str_replace(array('+', '/', '='), array('!', '.', '~'), $code);
		$table = "reset_codes";
		$data = array(
			'code'				=>		$code,
			'code_encoded'		=>		$code_encoded,
			'date_creation'		=>		time(),
			'user'				=>		$id
		);
		if($this->mdlauth->insert_somedata($table,$data)){
			return $code_encoded;
		}else{
			return false;
		}
	}
	/**
	 * Recupère l'identifiant d'un utiliateur à partir de son mail
	 * @param  string $email Email entré dans le champ
	 * @return bbol/string        Renvoie false si Erreur et la chaîne de mail si succès
	 */
	public function get_id_from_email($email=null){
		if($email == null){
			return false;
		}else{
			$table = "users";
			$field = array('id');
			$where = array(
				'email' => $email
			);
			$result = $this->mdlauth->get_somewhereresults($table,$field,$where);
			If($result->num_rows() == 0){
				return false;
			}else{
				$id = $result->row()->id;
				return $id;
			}
		}
	}
	/**
	 * Confirme la reinitialisation du mot de passe
	 * @param  string $code Code de reinitialisation
	 * @return void       Envoie un mail avec mot de passe en succès et affiche un message d'erreur en echec
	 */
	public function confirm_reset_password($code=null){
		if(!$this->ion_auth->logged_in()){
			if($code == null){
				$data['message_reset_password'] = "Le lien de reinitialisation n'est pas fourni, veuillez vous assurer de ne pas avoir commis d'erreurs ou recommencez la procédure de reinitialisation";
			}else{
				$code_decoded = str_replace(array('!', '.', '~'), array('+', '/', '='), $code);
				$decode = $this->encryption->decrypt($code_decoded);
				$id = $decode;
				$where = array(
					'id'		=>		$id
				);
				$result = $this->mdlauth->get_allwhereresults('users',$where);
				if($result->num_rows() != 0 && $this->check_reset_code($code) === true){
					$newpassword = random_string('alnum', 7);
				    $datasent = array(
				          'password' => $newpassword,
				           );
				    if($this->ion_auth->update($id, $datasent) == true){
				    	$data['message_reset_password'] = "La reinitialisation de votre mot de passe a bien été confirmée, vous recevrez un mail avec le nouveau mot de passe généré automatiquement. Servez vous en pour vous connecter, et ensuite changez le!";
				    	$data['class_success'] = 'alert-success';
				    	$datasend = array(
							'from'			=> 		'noreply@artconnectafrica.com',
							'to'			=>		array(
								[
									'address' => $this->ion_auth->user($id)->row()->email
								]
							),
							'subject'		=> 		'Votre nouveau mot de passe ',
							'body'			=>		'<!doctype html> <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> <head> <title>Votre nouveau mot de passe - Art Connect Africa </title> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <style type="text/css"> #outlook a{padding:0;}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table, td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}img{border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}p{display:block;margin:13px 0;}</style><!--[if mso]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml><![endif]--><!--[if lte mso 11]> <style type="text/css"> .mj-outlook-group-fix{width:100% !important;}</style><![endif]--> <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"> <style type="text/css"> @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700); </style> <style type="text/css"> @media only screen and (min-width:480px){.mj-column-per-100{width:100% !important; max-width: 100%;}.mj-column-per-50{width:50% !important; max-width: 50%;}.mj-column-per-90{width:90% !important; max-width: 90%;}}</style> <style type="text/css"> @media only screen and (max-width:480px){table.mj-full-width-mobile{width: 100% !important;}td.mj-full-width-mobile{width: auto !important;}}</style> </head> <body> <div style="" ><!--[if mso | IE]> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> </tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-top:#9f170b 5px solid;direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-left:0px;word-break:break-word;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;" > <tbody> <tr> <td style="width:600px;"> <img height="auto" src="https://mails.artconnectafrica.com/inscription/mailbanneraca.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600"/> </td></tr></tbody> </table> </td></tr></table> </div><!--[if mso | IE]> </td><td class="" style="vertical-align:top;width:300px;" ><![endif]--> <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" ><strong>Votre nouveau mot de passe </strong></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="word-wrap-outlook" style="vertical-align:top;width:540px;" ><![endif]--> <div class="mj-column-per-90 mj-outlook-group-fix word-wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left:#d65c13 10px solid;vertical-align:top;" width="100%" > <tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Vous avez solliciter une reinitialisation de votre mot de passe pour le compte '.$result->row()->username.' ('.$result->row()->email.') .</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Voici le mot de passe temporaire qui a été généré automatiquement : <b>'.$newpassword.'</b></div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Nous vous conseillons après que vous vous soyez authentifié de le changer dans votre compte et que le nouveau créé soit également un mot de passe de 7 caractères alphanumériques (lettres et chiffres) au moins afin de vous garantir une sécurité minimale.</div></td></tr><tr> <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;" >Cordialement!</div></td></tr><tr> <td align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;" > <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;" ><br></br></div></td></tr></table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </div></body> </html> '
						);
						$datasend = http_build_query($datasend);
						$url = base_url().'api/mail/sendmailnotls';
						$ch = curl_init($url);
						curl_setopt_array($ch, [
							CURLOPT_RETURNTRANSFER			=>			true,
							CURLOPT_POST					=>			1,
							CURLOPT_POSTFIELDS				=>			$datasend
						]);
						$datacurl = curl_exec($ch);
						if($datacurl === false){
							var_dump(curl_error($ch));
						}else{
							if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200){
								
							}else{
								
							}
						}
						curl_close($ch);
				    }else{
				    	$data['message_inscription'] = "Une erreur est survenue, peut-être que le lien d'activation donné n'est pas valide, veuillez vous assurer de ne pas avoir commis d'erreurs ou recommencez la procédure d'inscription";
				    }
				}else{
					$data['message_reset_password'] = "Une erreur est survenue, il semble que le lien de reinitialisation est expiré ou incorrecte, veuillez vous assurer de ne pas avoir commis d'erreurs et recommencez la procédure de reinitialisation";
				}
			}
			$data['metatitle'] = "Confirmation Reinitialisation du mot de passe";
			$data['module'] = "auth";
			$data['view'] = "reset_password_confirmation";
			echo Modules::run('template/auth', $data);
		}else{
			redirect(base_url());
		}
	}
	/**
	 * vérification du code de reinitialiation
	 * @param  string $code Code de reinitialisation
	 * @return bool       true si le code existe et a moins de 24h / false si le code n'existe pas
	 */
	public function check_reset_code($code=null){
		if($code === null){
			return false;
		}else{
			$table = 'reset_codes';
			$field = array('date_creation', 'user');
			$where = array(
				'code_encoded'	=>	$code,
			);
			$resultquery = $this->mdlauth->get_somewhereresults($table,$field,$where);
			if($resultquery->num_rows() == 0){
				return false;
			}else{
				$row = $resultquery->row();
				$date_creation = $row->date_creation;
				if((time() - $date_creation > 60*60*24)){
					return false;
				}else{
					return true;
				}
			}
		}
	}
}