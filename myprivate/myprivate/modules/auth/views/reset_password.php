<?php 
$csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
);
?>
<h2 class="text-center">Demande de modification de mot de passe</h2>
<form class="m-t" role="form" action="<?php echo base_url(); ?>auth/reset-password" method="post">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    <div class="form-group">
        <input type="email" name="email" class="form-control" placeholder="Email" required="" value="<?php echo set_value('email'); ?>">
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Reset</button>

    <a href="<?php echo base_url(); ?>auth/login">
        <small>Already have an account?</small>
    </a>

    <p class="text-muted text-center">
        <small>Do not have an account?</small>
    </p>
    <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url(); ?>auth/inscription">Create an account</a>
    <?php if(isset($message_reset_password)): ?>
    <br>
    <div class="alert <?php echo $class_success ?? 'alert-danger'; ?>">
        <?php echo validation_errors(); ?>
        <?php echo $message_reset_password; ?>
    </div>
    <?php endif; ?>
</form>