<h2 class="text-center">Confirmation d'inscription</h2>
<div class="">
    <div class="alert <?php echo $class = $inscriptionsuccessclass ?? 'alert-warning'; ?>">
        <?php echo $message_inscription ?? null; ?>
    </div>
</div>
<br>
<a class="btn btn-sm btn-white btn-block" href="<?php echo base_url(); ?>auth/login">Login</a>