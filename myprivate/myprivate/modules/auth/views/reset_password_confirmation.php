<?php 
$csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
);
?>
<h2 class="text-center">Nouveau mot de passe</h2>
<form class="m-t" role="form" action="<?php echo base_url(); ?>auth/reset-password" method="post">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

    <?php if(isset($message_reset_password)): ?>
    <br>
    <div class="alert <?php echo $class_success ?? 'alert-danger'; ?>">
        <?php echo $message_reset_password; ?>
    </div>
    <?php endif; ?>

    <br>

    <a href="<?php echo base_url(); ?>auth/login">
        <small>Already have an account?</small>
    </a>

    <p class="text-muted text-center">
        <small>Do not have an account?</small>
    </p>
    <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url(); ?>auth/inscription">Create an account</a>
</form>