<?php 
$csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
);
?>
<h2 class="text-center">Inscription</h2>
<form class="m-t" role="form" action="<?php echo base_url(); ?>auth/inscription" method="post">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" required="" name="email" value="<?php echo set_value('email'); ?>">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="Username" required="" name="username" value="<?php echo set_value('username'); ?>">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" required="" name="password">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" placeholder="Password Again" required="" name="repassword">
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Inscription</button>

    <a href="<?php echo base_url(); ?>auth/reset-password">
        <small>Forgot password?</small>
    </a>

    <p class="text-muted text-center">
        <small>Already have an account?</small>
    </p>
    <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url(); ?>auth/login">Login</a>
    <br>
    <?php if(isset($errors)): ?>
    <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors(); ?>
        <?php echo $errors; ?>
    </div>
    <?php endif; ?>
</form>