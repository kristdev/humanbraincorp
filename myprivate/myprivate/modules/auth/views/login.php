<?php 
$csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
);
?>
<h2 class="text-center">Connexion</h2>
<form class="m-t" role="form" action="<?php echo base_url(); ?>auth/login" method="post">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" required="" name="email" value="<?php echo set_value('email'); ?>">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" required="" name="password">
    </div>
    <div class="form-group text-center">
        <label> <input type="checkbox" value="true" name="remember"> Remember me </label>
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

   <!--  <a href="<?php echo base_url(); ?>auth/reset-password">
        <small>Forgot password?</small>
    </a>

    <p class="text-muted text-center">
        <small>Do not have an account?</small>
    </p>
    <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url(); ?>auth/inscription">Create an account</a> -->
    <br>
    <?php if(isset($errors)): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors(); ?>
        <?php echo $errors; ?>
    </div>
    <?php endif; ?>
</form>