<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_users extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
		$this->load->helper('date');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('users');
		$crud->set_subject('utilisateur');
		/**
		 *  N à N
		 */
		$crud->set_relation_n_n('group', 'users_groups', 'groups', 'user_id', 'group_id', 'name');
		/**
		 * `Colonnes à afficher
		 */
		$crud->columns('username','email','created_on','first_name','last_name');
		// $crud->unset_columns(array('ip_address','activation_selector','activation_code','forgotten_password_selector','forgotten_password_code','forgotten_password_time','remember_selector',
		// 	'remember_code','last_login','company','phone'));
		/**
		 * Opérations diverses
		 */
		$crud->callback_column('created_on', array($this,'formatdate'));

		$crud->required_fields('username');

		$crud->unset_fields('ip_address', 'activation_selector', 'activation_code','forgotten_password_selector','forgotten_password_code','forgotten_password_time','remember_selector','remember_code','last_login');


        $crud->callback_read_field('created_on', function ($value, $primary_key) {
            return date("y-m-d h:i:s", $value);
        });

		$crud->callback_edit_field('password', function ($value, $primary_key) {
            return '<input type="text" maxlength="50" value="'.$value.'" class="form-control" name="password" disabled>';
        });

        $crud->callback_edit_field('created_on', function ($value, $primary_key) {
        	$newvalue = date("y-m-d h:i:s", $value);
            return '<input type="text" maxlength="50" value="'.$newvalue.'" class="form-control" name="created_on" disabled>';
        });

        $crud->callback_edit_field('username', function ($value, $primary_key) {
            return '<input type="text" maxlength="50" value="'.$value.'" class="form-control" name="created_on" disabled>';
        });

        $crud->callback_add_field('created_on', function(){
        	$newvalue = mdate("%y-%m-%d %h:%i:%s", time());
        	return '<input type="text" maxlength="50" value="'.$newvalue.'" class="form-control" name="created_on" disabled>';
        });

        $crud->callback_insert(array($this,'registeruser'));

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

	/**
	 * Fonction de formattage de la date
	 * @param  int $value date entrée
	 * @param  [type] $row   [description]
	 * @return date        dat formatée selon le modèle défini
	 */
	public function formatdate($value, $row){
		return date("y-m-d h:i:s", $value);
	}

	/**
	 * Enregistrement d'un nouvel utilisateur
	 * @param  Array $postdata Données de formulaire
	 * @return [type]           [description]
	 */
	public function registeruser($postdata){
		$username = $postdata['username'];
	    $password = $postdata['password'];
	    $email = $postdata['email'];
	    $additional_data = array(
	                'first_name' => $postdata['first_name'],
	                'last_name' => $postdata['last_name'],
	                'company' => $postdata['company'],
	                'phone' => $postdata['phone'],
	                );
	    $group = array('1'); // Sets user to admin.

	    $this->ion_auth->register($username, $password, $email, $additional_data, $group);
	}

}

?>