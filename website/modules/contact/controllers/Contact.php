<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->helper('captcha');
		$this->load->helper('url');
		$this->load->helper('string');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function index(){
		$data['metatitle'] = "Nous contacter";
		$data['module'] = "contact";
		$data['view'] = "contactpage";

		
		
		$vals = array(
		    
		    'img_path'    		=> APPPATH.'../myprivate/assets/captcha/',
		    'img_url'    		=> base_url().'myprivate/assets/captcha/',
		    'img_width'    		=> 150,
		    'img_height' 		=> 30,
		    'word_length'   	=> 5,
		    'pool'				=> '0123456789',
		    'expiration' 		=> 900,
		    'colors'     		=> array(
		                    'background' => array(255, 255, 255),
		                    'border' => array(255, 255, 255),
		                    'text' => array(0, 0, 0),
		                    'grid' => array(255, 255, 255)
		            )
		    );

		$data['cap'] = create_captcha($vals);
		$this->session->set_flashdata('captcha', $data['cap']['word']);
		echo Modules::run('template/home', $data);
	}


	public function send(){
		// var_dump($this->input->post());
		// echo $this->input->ip_address();
		if($this->input->post() == null){
			$this->index();
		}else{	
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('captcha', 'Captcha', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['error_form'] = "Un champ a été mal rempli, veuillez vérifier les données";
				$data['metatitle'] = "Nous contacter";
				$data['module'] = "contact";
				$data['view'] = "contactpage";

				
				
				$vals = array(
				    
				    'img_path'    		=> APPPATH.'../myprivate/assets/captcha/',
				    'img_url'    		=> base_url().'myprivate/assets/captcha/',
				    'img_width'    		=> 150,
				    'img_height' 		=> 30,
				    'word_length'   	=> 5,
				    'pool'				=> '0123456789',
				    'expiration' 		=> 900,
				    'colors'     		=> array(
				                    'background' => array(255, 255, 255),
				                    'border' => array(255, 255, 255),
				                    'text' => array(0, 0, 0),
				                    'grid' => array(255, 255, 255)
				            )
				    );

				
				$data['cap'] = create_captcha($vals);
				$this->session->set_flashdata('captcha', $data['cap']['word']);
				echo Modules::run('template/home', $data);
			}else{
				if($this->checkcaptcha($this->input->post('captcha')) == true){

					$datapost['inputpost'] = $this->input->post();
					$datapost['module'] = "contact";
					$datapost['view'] = "mailcontact";
					$this->load->module('sendmail');
					if($this->sendmail->html($datapost) == true){
						$data['success_form'] = "Votre message a été envoyé, vous serez contacté par notre service support.";
						$data['metatitle'] = "Nous contacter";
						$data['module'] = "contact";
						$data['view'] = "contactpage";

						
						
						$vals = array(
						    
						    'img_path'    		=> APPPATH.'../myprivate/assets/captcha/',
						    'img_url'    		=> base_url().'myprivate/assets/captcha/',
						    'img_width'    		=> 150,
						    'img_height' 		=> 30,
						    'word_length'   	=> 5,
						    'pool'				=> '0123456789',
						    'expiration' 		=> 900,
						    'colors'     		=> array(
						                    'background' => array(255, 255, 255),
						                    'border' => array(255, 255, 255),
						                    'text' => array(0, 0, 0),
						                    'grid' => array(255, 255, 255)
						            )
						    );

						$data['cap'] = create_captcha($vals);
						$this->session->set_flashdata('captcha', $data['cap']['word']);
						echo Modules::run('template/home', $data);
					}else{
						$data['error_form'] = "une erreur est survenue pendant l'envoie de mail, veuillez recommencer l'opération!";
						$data['metatitle'] = "Nous contacter";
						$data['module'] = "contact";
						$data['view'] = "contactpage";

						
						
						$vals = array(
						    'word'   	 		=> 'Random Word',
						    'img_path'    		=> APPPATH.'../myprivate/assets/captcha/',
						    'img_url'    		=> base_url().'myprivate/assets/captcha/',
						    'img_width'    		=> 150,
						    'img_height' 		=> 30,
						    'word_length'   	=> 5,
						    'pool'				=> '0123456789',
						    'expiration' 		=> 900,
						    'colors'     		=> array(
						                    'background' => array(255, 255, 255),
						                    'border' => array(255, 255, 255),
						                    'text' => array(0, 0, 0),
						                    'grid' => array(255, 255, 255)
						            )
						    );

						$data['cap'] = create_captcha($vals);
						$this->session->set_flashdata('captcha', $data['cap']['word']);
						echo Modules::run('template/home', $data);
					}
				}else{
					$data['captcha_error'] = 'Le nombre inscrit ne correspond pas à celui proposé, veuillez recommencer!';
					$data['error_form'] = "une erreur est survenue pendant l'envoie de mail, veuillez recommencer l'opération!";
					$data['metatitle'] = "Nous contacter";
					$data['module'] = "contact";
					$data['view'] = "contactpage";

					
					
					$vals = array(
					    
					    'img_path'    		=> APPPATH.'../myprivate/assets/captcha/',
					    'img_url'    		=> base_url().'myprivate/assets/captcha/',
					    'img_width'    		=> 150,
					    'img_height' 		=> 30,
					    'word_length'   	=> 5,
					    'pool'				=> '0123456789',
					    'expiration' 		=> 900,
					    'colors'     		=> array(
					                    'background' => array(255, 255, 255),
					                    'border' => array(255, 255, 255),
					                    'text' => array(0, 0, 0),
					                    'grid' => array(255, 255, 255)
					            )
					    );

					$data['cap'] = create_captcha($vals);
					$this->session->set_flashdata('captcha', $data['cap']['word']);
					echo Modules::run('template/home', $data);
				}
			}
		}
	}


	public function checkcaptcha($captcha){
		if($captcha == $this->session->flashdata('captcha')){
			return true;
		}else{
			return false;
		}
	}

}

?>