<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-70" data-jarallax data-speed="0.7" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/cover-help.jpg);">

    <div class="container py-5">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-7 col-md-10  text-center">
                <h1 class="display-4 mb-4 text-white">Une préocupation? Besoin de profils? <br> HBC vous répond</h1>
                <!-- <p class="text-white-90 mb-0 lead"> </p> -->
            </div>
        </div>
    </div>

</section>

<!-- CONTACT
================================================== -->
<section class="border-bottom border-light">
    <div class="container">

        <!-- Contact box -->
        <div class="row">

            <div class="col-md-4 mb-5 mb-md-0 text-center transition-hover">
                <div class="card py-5 rounded h-100">
                    <div class="card-body">
                        <div class="mb-3">
                            <img class="feather display-6" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/svg/mail.svg" alt="...">
                        </div>
                        <h5 class="mb-2">Email</h5>
                        <p class="mb-0">ninangougue@humanbraincorp.com</p>
                        <p class="mb-0">stephanefoka@humanbraincorp.com</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-5 mb-md-0 text-center transition-hover">
                <div class="card py-5 border-transparent bg-primary h-100">
                    <div class="card-body">
                        <div class="mb-3">
                            <img class="feather display-6 text-white" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/svg/map-pin.svg" alt="...">
                        </div>
                        <h5 class="mb-2 text-white">Adresse</h5>
                        <p class="mb-0 text-white">Bonamousadi...</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 text-center transition-hover">
                <div class="card py-5 rounded h-100">
                    <div class="card-body">
                        <div class="mb-3">
                            <img class="feather display-6" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/svg/phone.svg" alt="...">
                        </div>
                        <h5 class="mb-2">Tel</h5>
                        <p class="mb-0">(+237) 653 689 588</p>
                        <p class="mb-0">(+237) 691 305 258</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- End Contact box -->

    </div>
</section>

<!-- CONTACT FORM
================================================== -->
<section>
    <div class="container">

        <div class="row justify-content-center mb-5 mb-md-7">
            <div class="col-md-10 col-lg-7 text-center">

                <div class="badge badge-pill badge-primary-soft mb-3">
                    <span>Contactez-nous</span>
                </div>
                <h3 class="font-weight-normal line-height-base"> <span class="emphasis">Laissez-nous un message! Nous faisons le reste!</span></h3>

            </div>
        </div>

        <!-- Form -->
        <div class="row justify-content-center">

            <div class="col-md-10">

                <!-- start form here -->

                <?php 
                    if(isset($error_form)):
                ?>
                <div class="alert alert-danger" role="alert">
                   <?php echo $error_form; ?>
                </div>
                <?php
                    endif;
                   if(isset($success_form)):
                ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $success_form; ?>
                </div>
                <?php endif; ?>


                <form class="" action="<?php echo base_url(); ?>contact/send" method="post" enctype="multipart/form-data" onclick="">

                    <div class="quform-elements">

                        <div class="row">

                            <!-- Begin Text input element -->
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="name">Votre nom</label>
                                    <div class="">
                                        <input class="form-control" id="name" type="text" name="name" placeholder="Your name here" required="" value="<?php echo set_value('name'); ?>" />
                                    </div>
                                </div>

                            </div>
                            <!-- End Text input element -->

                            <!-- Begin Text input element -->
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="email">Votre email</label>
                                    <div class="">
                                        <input class="form-control" id="email" type="text" name="email" placeholder="Your email here" required="" value="<?php echo set_value('email'); ?>" />
                                    </div>
                                </div>
                            </div>
                            <!-- End Text input element -->

                            <!-- Begin Text input element -->
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="company">Nom structure</label>
                                    <div class="">
                                        <input class="form-control" id="company" type="text" name="company" placeholder="Your company name" value="<?php echo set_value('company'); ?>" />
                                    </div>
                                </div>

                            </div>
                            <!-- End Text input element -->

                            <!-- Begin Text input element -->
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="phone">Numéro de tel</label>
                                    <div class="">
                                        <input class="form-control" id="phone" type="text" name="phone" placeholder="123456789" value="<?php echo set_value('phone'); ?>" />
                                    </div>
                                </div>

                            </div>
                            <!-- End Text input element -->

                            <!-- Begin Textarea element -->
                            <div class="col-md-12">
                                <div class=" form-group">
                                    <label for="message">Message</label>
                                    <div class="">
                                        <textarea class="form-control" id="message" name="message" rows="5" placeholder="Tell us a few words" required="" value="<?php echo set_value('message'); ?>"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- End Textarea element -->

                            <!-- Begin Captcha element -->
                            <div class="col-md-12">
                                <div class="">
                                    <div class="form-group">
                                        <div class="quform-captcha">
                                            <div class="">
                                                Soit le nombre suivant: <?php echo $cap['image']; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="">
                                            <input class="form-control" id="captcha" type="number" name="captcha" placeholder="Recopiez le nombre ci-dessus" required="" value="<?php echo set_value('captcha'); ?>" />
                                        </div>
                                        <?php 
                                            if(isset($captcha_error)):
                                        ?>
                                            <p class="text-danger"><?php echo $captcha_error;?></p>
                                        <?php 
                                            endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End Captcha element -->

                            <!-- Begin Submit button -->
                            <div class="col-md-12">
                                <div class="">
                                    <button class="btn btn-primary btn-md" type="submit"><span>Send Message</span></button>
                                </div>
                            </div>
                            <!-- End Submit button -->

                        </div>

                    </div>

                </form>

                <!-- end form here -->

            </div>
        </div>
        <!-- End Form -->

    </div>
</section>