<!-- BLOG
================================================== -->
<section class="pt-0">
    <div class="container">
        <div class="text-center mb-5 mb-lg-7">
            <h3 class="h1"><span class="emphasis">Quelques articles du blog</span></h3>
        </div>

        
        <div class="row no-gutters d-flex justify-content-center">
            <?php  
            $articlesquery = $query;
            $articles = $query->result();
            if( $articlesquery->num_rows() === 0):
            ?>
            <div>
                <h3 class="text-center">Aucun article n'est encore configuré!</h3>
            </div>
            <?php 
            else:
            foreach($articles as $article):
            ?>
            <div class="col-md-6 col-lg-4 mb-4 mb-md-5 mb-lg-0">
                <div class="px-0 px-md-3 px-lg-2 px-xl-4">
                    <div class="card border-0 text-white h-350 h-lg-400 jarallax overlay overlay-gradient border-radius-before" data-jarallax data-speed="1" style="background-image: url(<?php echo base_url().'myprivate/assets/uploads/blog/baniere/'.$article->baniere; ?>);">

                        <div class="row no-gutters h-100 p-4 p-lg-6">
                            <div class="col-12">
                                <a class="text-white bg-primary badge badge-pill" href="<?php echo base_url(); ?>blog/category/<?php echo Modules::run('blog/showslugcategory', $article->categories_blog_id);?>"><?php echo Modules::run('blog/showcategory', $article->categories_blog_id); ?></a>
                            </div>
                            <div class="col-12 align-self-end">
                                <h3 class="card-title h5 mb-3">
                                    <a class="text-white" href="<?php echo base_url().'blog/article/'.$article->slug; ?>"><?php echo $article->titre; ?></a>
                                </h3>

                                <div class="media align-items-center">
                                    <!-- <img class="sm-avatar rounded-circle mr-3" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/avatar/avatar-02.jpg" alt="..."> -->
                                    <div class="media-body"><?php echo unix_to_human($article->created, TRUE, 'eu');?></div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php 
            endforeach;
            endif;
            ?>

        </div>
        
        <div class="row justify-content-center mt-3 mt-md-4">
            <div class="col-auto">
                <div class="alert bg-warning text-white rounded mb-0">
                    <a href="https://humanbraincorp.com/blog" class="font-weight-bold text-white">Consultez les articles du blog</a>
                </div>
            </div>
        </div>
    </div>
</section>