<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blognews extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->model('Mdl_blognews', 'blogtab');
		$this->load->helper('date');
	}

	public function index(){
		$query = $this->listlastarticles();
		$data['query'] = $query;
		$this->load->view('blognewspage', $data);
	}

	public function listlastarticles(){
		$table = "post_blog";
		$limit = 3;
		$offset = 0;
		$orderby = 'id';
		$set = 'DESC';
		$query = $this->blogtab->get_allbornedresults($table,$limit,$offset,$orderby,$set);
		return $query;
	}

}

?>