<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gc_cv extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->config('grocery_crud');
		$this->load->helper('date');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
	}
	/**
	 * Listiing des utilisateurs
	 * @return [type] [description]
	 */
	public function index(){
		$crud = new grocery_CRUD();
		/**
		 * Définition de la table à affichier et du sujet
		 */
		$crud->set_table('cv');
		$crud->where('candidat_id', $this->ion_auth->user()->row()->id);
		$crud->set_subject('CV');
		$crud->set_field_upload('file','myprivate/assets/uploads/cv');
		$crud->columns(['file','libelle','date']);
		$crud->add_fields(array('file','libelle','description','candidat_id'));
		$crud->field_type('candidat_id', 'hidden', $this->ion_auth->user()->row()->id);
		$crud->unset_edit_fields('date');

        /**
         * Sortie ou affichage
         * @var [type]
         */
		$output = $crud->render();

		$this->load->view('output',$output);
	}

}

?>