<!-- ABOUT US
================================================== -->
<section>
    <div class="container">
        <div class="row align-items-center justify-content-around mb-9 mb-lg-15">
            <div class="col-md-9 col-lg-5 col-xl-5 text-center mb-5 mb-lg-0 d-none d-lg-block">
                <img class="rounded" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/about.jpg" alt="creativity img">
            </div>
            <div class="col-md-9 col-lg-6 col-xl-5">

                <h4 class="h2 mb-4"><span class="emphasis">Testez notre créativité</span></h4>
                <!-- <p class="lead">We included feature items gives you possibility to build any kind of website.</p> -->
                <ul class="list-unstyled mb-5 p-0">
                    <li class="py-2">
                        <div class="d-flex">
                            <span class="list-style1 mr-3">
                                    <i class="fas fa-check"></i>
                            </span>
                            L’innovation au cœur de notre démarche
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex">
                            <span class="list-style1 mr-3">
                                    <i class="fas fa-check"></i>
                            </span> 
                            Gestion des projets professionnel et d’entreprise centrée sur l’humain
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex">
                            <span class="list-style1 mr-3">
                                    <i class="fas fa-check"></i>
                            </span> 
                            Coaching situationnel 
                        </div>
                    </li>
                </ul>

                <a href="<?php echo base_url(); ?>expertises" class="btn btn-light-primary btn-md">Nos prestations <span class="fa fa-angle-right ml-2 align-middle"></span></a>

            </div>
        </div>

        <!-- <div class="row justify-content-center text-center">
            <div class="col-4 col-md-2">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/partners/01.png" class="px-0 px-sm-2 opacity4 hover-opacity" alt="...">
            </div>
            <div class="col-4 col-md-2">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/partners/02.png" class="px-0 px-sm-2 opacity4 hover-opacity" alt="...">
            </div>
            <div class="col-4 col-md-2">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/partners/03.png" class="px-0 px-sm-2 opacity4 hover-opacity" alt="...">
            </div>
            <div class="col-4 col-md-2">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/partners/04.png" class="px-0 px-sm-2 opacity4 hover-opacity" alt="...">
            </div>
            <div class="col-4 col-md-2">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/partners/05.png" class="px-0 px-sm-2 opacity4 hover-opacity" alt="...">
            </div>

        </div> -->

    </div>
</section>