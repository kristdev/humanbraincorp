<!-- VISION  &  MISSION
================================================== -->
<section class="pt-5">
    <div class="container">
        <div class="row align-items-center justify-content-center no-gutters">

            <!-- Our vision -->
            <div class="col-md-9 col-lg-9 mb-5 mb-md-0">
                <div class="card px-3 px-lg-4 mr-0 border-0 card-standard shadow-light">
                    <div class="card-body bg-transparent py-6 px-0 border-0">
                        <h2 class="h3 mb-5 pb-4 border-bottom text-center">Notre mission</h2>
                        <p class="lead px-0 px-sm-4 mb-4">
                        Dans une démarche de partenariat stratégique, HBC vous accompagne de la conception de votre projet professionnel ou d’entreprise jusqu’à sa mise en œuvre avec pour but l’atteinte de vos objectifs stratégiques.
                        </p>

                        <ul class="">
                            <li>Assurer toutes les différentes étapes du processus de recrutement pour arrimer profil et mission.</li>
                            <li>Poser un diagnostic, rechercher et proposer des solutions pour améliorer le fonctionnement de l'Entreprise ou de l'Organisation.</li>
                            <li>Aider l’entreprise à booster la motivation des salariés en adaptant le projet de la structure aux contraintes de l’environnement.</li>
                            <li>Développer l’esprit d’initiative et d’équipe chez les salariés.</li>
                            <li>Arrimer les actions en lien avec le capital humain afin d’atteindre les objectifs de l’organisation.</li>
                            <li>Aider les entreprises à structurer  et administrer le département RH.</li>
                            <li>Aider les entreprise et organisation à planifier leurs besoins de compétences, et à identifier les stratégies appropriées.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Our vision -->

            <!-- Our mission -->
            <div class="col-md-9 col-lg-9">
                <div class="card px-3 px-lg-4 ml-0 mr-0 border-0 bg-primary card-enterprise shadow-light">
                    <div class="card-body bg-transparent py-6 px-0 text-center border-0">
                        <h2 class="h3 mb-5 pb-4 border-bottom border-light-white text-white">Nos valeurs</h2>

                        <div class="max-width-content mx-auto mb-0 px-2 text-left text-white">
                            <h5 class="text-white">Respect</h5>
                            <p class="px-0 px-sm-4 mb-4">Ecoute, Bienveillance et Respect des délais sont au cœur du traitement de vos attentes.</p>

                            <h5 class="text-white">Satisfaction Client</h5>
                            <p class="px-0 px-sm-4 mb-4">Pour chaque mission à nous confier, notre objectif est de satisfaire en priorité  vos besoins. Un suivi post-mission est également envisagé dans le cadre notre accompagnement.</p>

                            <h5 class="text-white">Pédagogie </h5>
                            <p class="px-0 px-sm-4 mb-4">Nous avons pour ambition de vulgariser les pratiques Rh et les rendre plus compréhensibles par toute personne intéressée par la discipline ou la fonction. </p>
                            
                        </div> 

                    </div>
                </div>
            </div>
            <!-- End Our mission -->

        </div>
    </div>
</section>