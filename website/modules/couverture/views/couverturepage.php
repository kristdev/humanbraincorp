<!-- BANNER
================================================== -->
<div class="main-slider bg-light">

    <div class="container">

        <div class="row align-items-stretch">

            <div class="col-12 col-lg-6 offset-lg-1 order-2 pb-9 pb-lg-0">

                <div class="position-relative h-100 min-height-50vh vw-lg-50 slider-section">

                    <div class="header-shape-bg d-none d-lg-block">

                        <svg xmlns="http://www.w3.org/2000/svg" height="100%" viewBox="0 0 105 673">
                            <path class="fill-light" d="M83.8,680S-39.5,388.8,83.8,0H0V680Z" transform="translate(0 0)"></path>
                            <path class="fill-primary" d="M105,680S-29.1,398,105,0H82.2s-119.8,324.2,0,680Z" transform="translate(0 0)"></path>
                        </svg>

                    </div>

                    <!-- Slider -->
                    <div class="owl-carousel owl-theme h-100 w-100">
                        <?php 
                        $queryimages = $imagesgalleryquery;
                        $queryimagesresult = $queryimages->result();
                        foreach($queryimagesresult as $image){
                            ?>
                        <div class="item w-100 h-100 bg-cover" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/uploads/couverture/<?php echo $image->path; ?>);">

                        </div>
                            <?php
                        }
                        ?>


                    </div>

                </div>

            </div>

            <div class="col-12 col-md-10 col-lg-5 min-height order-1 py-9 py-lg-0">

                <div class="row no-gutters align-items-center h-100">
                    <div class="col-12">
                        <div class="badge badge-pill badge-primary-soft mb-3">
                            <span><?php echo $couverture->drapeau; ?></span>
                        </div>

                        <h1 class="mb-4 line-height-sm"> <?php echo $couverture->libelle; ?>  <!-- La <strong class="text-warning font-weight-bold">Gestion</strong> des équipes. --></h1>
                        <p class="lead mb-5 mb-xl-6 line-height-lg w-md-90 w-lg-100">
                            <?php echo $couverture->description; ?>
                        </p>


                        <a href="<?php echo base_url().$couverture->link; ?>" target="_blank" class="btn btn-primary btn-md mr-2 my-1 my-sm-0">Plus de détails</a>
                        <a href="<?php echo base_url(); ?>about" class="btn btn-light-primary btn-md my-1 my-sm-0">A propos de nous</a>

                    </div>
                </div>

            </div>

        </div>

    </div>

</div>