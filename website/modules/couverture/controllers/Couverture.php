<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Couverture extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->model('Mdl_couverture', 'couverturetab');
	}

	public function index(){
		
	}

	public function show($visibilite = 0){
		$table = 'couverture';
		$where = array(
			'visibilite' => $visibilite
		);
		$visibilitequery = $this->couverturetab->get_allwhereresults($table,$where);
		if(isset($visibilitequery)){
			$visibiliterow = $visibilitequery->row();
			$data['couverture'] = $visibiliterow;
			$imagesgalleryquery = $this->listgallery($visibiliterow->id);
			if(isset($imagesgalleryquery)){
				// echo '<h1>'.$imagesgalleryquery->num_rows().'</h1>';
				$data['imagesgalleryquery'] = $imagesgalleryquery;
			}
			$this->load->view('couverturepage', $data);
		}else{
			echo 'Couverture mal configurée';
		}
	}

	public function listgallery($idcouverture){
		$table = 'gallery_couverture';
		$where = array(
			'couverture_id' => $idcouverture
		);
		$galleryquery = $this->couverturetab->get_allwhereresults($table,$where);
		if(isset($galleryquery)){
			return $galleryquery;
		}
	}

}

?>