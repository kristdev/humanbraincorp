<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expertises extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->model('Mdl_expertises', 'expertisestab');
	}

	public function index(){
		$data['metatitle'] = "Nos domaines d'expertise";
		$data['module'] = "expertises";
		$data['view'] = "expertisespage";
		echo Modules::run('template/home', $data);
	}
	/**
	 * Affiche la liste des expertises
	 * @return mysql obect Liste des expertises
	 */
	public function list(){
		$table = "expertise";
		$where = array(
			'visibilite'	=> 'yes'
		);
		$query = $this->expertisestab->get_allwhereresults($table,$where);
		return $query;
	}


	public function show($slug = NULL){
		if($slug == NULL){
			$this->index();
		}else{
			$table = "expertise";
			$where = array(
				'visibilite'	=> 'yes',
				'slug'			=> $slug
			);
			$expertisequery = $this->expertisestab->get_allwhereresults($table,$where);
			$expertise = $expertisequery->row();

			if(isset($expertise)){				$data['metatitle'] = "Expertise - ".$expertise->libelle;
				$data['module'] = "expertises";
				$data['view'] = "expertisesshowpage";
				$data['arraydata'] = $expertise;
				echo Modules::run('template/home', $data);
			}else{
				$this->index();
			}

		}
	}
}