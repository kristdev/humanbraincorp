<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-60 bottom-shape" data-jarallax data-speed="0.8" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/expertise.jpg);">
    <div class="container pt-5 pb-4">

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 text-center">
                <h1 class="display-4 text-white font-weight-bold mb-3">Domaines d'expertise</h1>
                <!--<p class="lead text-white-90 mb-5 w-100 w-md-80 mx-auto">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto dolor inventore explicabo, error, cupiditate perspiciatis esse recusandae doloremque porro et, distinctio optio praesentium quod eveniet quas tempora magni soluta similique.
                </p>-->

                <!--<p class="mb-0 font-weight-bold text-white"><i class="fas fa-map-marker-alt text-warning mr-2 my-1 my-sm-0"></i> Chicago, US, Full Time. <span class="badge badge-pill badge-warning d-inline-block my-1 my-sm-0">$50k - $75k</span></p>-->

            </div>

        </div>

    </div>

    <!-- Bg shape -->
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-white" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>
    <!-- End Bg shape -->

</section>

<!-- EXPERTISES LISTE
================================================== -->
<section class="pt-5">
    <div class="container">

        <!-- Blog -->
        <div class="row">
            <?php  
            $expertisesquery = Modules::run('expertises/list');
            $expertises = $expertisesquery->result();
            ?>
            <?php
                if( $expertisesquery->num_rows === 0){
                    ?>
                    <h1 class="text-center">Aucune expertise n'est encore configurée!</h1>
                    <?php
                }
                foreach($expertises as $expertise){
                ?>    
                <div class="col-md-6 col-lg-4 mb-7">
                    <div class="card border-0 rounded-lg shadow-light h-100">
                        <img src="<?php echo base_url().'myprivate/assets/uploads/expertise/baniere/'.$expertise->baniere; ?>" class="card-img-top" alt="<?php echo $expertise->slug; ?>">
                        <div class="card-body p-5">
                            <div class="border-bottom border-light mb-4 pb-4">
                                <a href="#!"><h5><?php echo $expertise->libelle; ?></h5></a>
                                <p><?php echo $expertise->description; ?></p>
                                <a href="<?php echo base_url().'expertises/show/'.$expertise->slug ; ?>">read more</a>
                            </div>
                            <!-- <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">$expertise->slug
                                    <img src="assets/img/testmonials/t-4.jpg" class="rounded-circle sm-avatar" alt="...">
                                    <div class="ml-3">
                                        <span class="small"><span class="font-weight-bold">By: </span>Rona Lewis</span>
                                    </div>
                                </div>
                                <a href="#!" class="bg-danger-10 py-1 px-2 rounded-pill text-danger small">
                                    <i class="fas fa-heart mr-1"></i>17
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            <?php 
                }
            ?>
        </div>
        <!-- End Blog -->

    </div>
</section>