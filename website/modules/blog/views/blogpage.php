
<!-- TITLE
================================================== -->
<section>
    <div class="container">

        <div class="row align-items-center justify-content-center">
            <div class="col-lg-10 col-xl-8 text-center">
                <h1 class="display-4 mb-4">Nos articles du blog</h1>
                <p class="mb-5 w-90 w-md-70 mx-auto">Découvrez les actualités, les astuces et nos activités! Restez connectés!</p>
                <!-- <form>
                    <div class="d-flex align-items-center">
                        <div class="input-group ie-width-80">
                            <input type="text" class="form-control" name="search" id="search" placeholder="Search blog">
                        </div>
                        <button type="submit" class="btn btn-primary text-nowrap ml-3 mb-0">
                            <span class="fas fa-search mr-2"></span> Search
                        </button>
                    </div>
                </form> -->
            </div>
        </div>

    </div>
</section>
</section>

<!-- EXPERTISES LISTE
================================================== -->
<section class="pt-5">
    <div class="container">

        <!-- Blog -->
        <div class="row">
            <?php  
            $articlesquery = Modules::run('blog/listarticles');
            $articles = $articlesquery->result();
            ?>
            <?php
                if( $articlesquery->num_rows() === 0){
                    ?>
                    <div>
                        <h1 class="text-center">Aucun article n'est encore configuré!</h1>
                    </div>
                    <?php
                }
                foreach($articles as $article){
                ?>    
                <div class="col-md-6 col-lg-4 mb-7">
                    <div class="card border-0 rounded-lg shadow-light h-100">
                        <img src="<?php echo base_url().'myprivate/assets/uploads/blog/baniere/'.$article->baniere; ?>" class="card-img-top" alt="<?php echo $article->slug; ?>">
                        <div class="card-body p-5">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                                <a href="#!" class="label font-weight-bold"><?php echo Modules::run('blog/showcategory', $article->categories_blog_id); ?></a>
                                <span class="small"><?php echo unix_to_human($article->created, TRUE, 'eu');?></span>
                            </div>
                            <div class="border-bottom border-light mb-4 pb-4">
                                <a href="<?php echo base_url().'blog/article/'.$article->slug; ?>"><h5><?php echo $article->titre; ?></h5></a>
                                <p><?php echo $article->intro; ?></p>
                                <a href="<?php echo base_url().'blog/article/'.$article->slug; ?>">read more</a>
                            </div>
                            <!-- <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <img src="assets/img/testmonials/t-4.jpg" class="rounded-circle sm-avatar" alt="...">
                                    <div class="ml-3">
                                        <span class="small"><span class="font-weight-bold">By: </span>Rona Lewis</span>
                                    </div>
                                </div>
                                <a href="#!" class="bg-danger-10 py-1 px-2 rounded-pill text-danger small">
                                    <i class="fas fa-heart mr-1"></i>17
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            <?php 
                }
            ?>
        </div>
        <!-- End Blog -->

    </div>
</section>