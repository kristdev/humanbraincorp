<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-60 bottom-shape" data-jarallax data-speed="0.8" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/uploads/blog/baniere/<?php echo $arraydata->baniere; ?>);">
    <div class="container pt-5 pb-4">

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 text-center">
                <h1 class="display-4 text-white font-weight-bold mb-3"><?php echo $arraydata->titre; ?></h1>
                <!-- <p class="lead text-white-90 mb-5 w-100 w-md-80 mx-auto">
                    <?php echo $arraydata->description; ?>
                </p> -->

                <!-- <p class="mb-0 font-weight-bold text-white"><i class="fas fa-map-marker-alt text-warning mr-2 my-1 my-sm-0"></i> Chicago, US, Full Time. <span class="badge badge-pill badge-warning d-inline-block my-1 my-sm-0">$50k - $75k</span></p> -->

            </div>

        </div>

    </div>

    <!-- Bg shape -->
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-white" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>
    <!-- End Bg shape -->

</section>

<!-- EXPERTISES LISTE
================================================== -->
<section class="pt-5">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-9">

                <article>
                    <?php echo $arraydata->body; ?>
                </article>
                <div id="disqus_thread"></div>
                <script>
                    /**
                    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                    /*
                    var disqus_config = function () {
                    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    */
                    (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = 'https://humanbraincorp-com.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
        </div>
    </div>
</section>