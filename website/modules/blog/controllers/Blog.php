<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->model('Mdl_blog', 'blogtab');
		$this->load->helper('date');
	}

	public function index(){
		$data['metatitle'] = "Le blog";
		$data['module'] = "blog";
		$data['view'] = "blogpage";
		echo Modules::run('template/home', $data);
	}
	/**
	 * Affiche la liste des catégories de blog
	 * @return mysql obect Liste des catégories de blog
	 */
	public function listcategories(){
		$table = "categories_blog";
		$where = array(
			'visibilite'	=> 'yes'
		);
		$query = $this->blogtab->get_allwhereresults($table,$where);
		return $query;
	}

	public function listarticles(){
		$table = "post_blog";
		$where = array(
			'visibilite'	=> 'yes'
		);
		$query = $this->blogtab->get_allwhereresults($table,$where);
		return $query;
	}


	public function category($category = null){
		if(isset($category)){
			$table = "categories_blog";
			$where = array(
				'slug'			=>	$category,
				'visibilite'	=> 'yes'
			);
			$query = $this->blogtab->get_allwhereresults($table,$where);
			$querynumrows = $query->num_rows();
			if($querynumrows != 0){
				$id = $query->row()->id;

				$table2 = "post_blog";
				$where2 = array(
					'visibilite'	=> 'yes',
					'categories_blog_id'	=> $id
				);
				$query2 = $this->blogtab->get_allwhereresults($table2,$where2);

				$data['metatitle'] = "Le blog, catégorie ".$query->row()->libelle;
				$data['module'] = "blog";
				$data['view'] = "blogcategoriespage";
				$data['dataposts'] = $query2;
				echo Modules::run('template/home', $data);
				return $query;
			}else{
				$this->index();
			}

		}else{
			$this->index();
		}
	}



	public function article($slug = NULL){
		if($slug == NULL){
			$this->index();
		}else{
			$table = "post_blog";
			$where = array(
				'visibilite'	=> 'yes',
				'slug'			=> $slug
			);
			$articlequery = $this->blogtab->get_allwhereresults($table,$where);
			$article = $articlequery->row();

			if(isset($article)){				
				$data['metatitle'] = "Article de blog - ".$article->titre;
				$data['module'] = "blog";
				$data['view'] = "articleshowpage";
				$data['arraydata'] = $article;
				echo Modules::run('template/home', $data);
			}else{
				$this->index();
			}

		}
	}

	public function showcategory($categoryid){
		$table = "categories_blog";
		$where = array(
			'id'			=>	$categoryid
		);
		$query = $this->blogtab->get_allwhereresults($table,$where);
		$rowdata = $query->row();
		$category = $rowdata->libelle;
		return $category;
	}

	public function showslugcategory($categoryid){
		$table = "categories_blog";
		$where = array(
			'id'			=>	$categoryid
		);
		$query = $this->blogtab->get_allwhereresults($table,$where);
		$rowdata = $query->row();
		$category = $rowdata->slug;
		return $category;
	}
}