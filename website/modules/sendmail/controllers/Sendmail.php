<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendmail extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->library('email');
	}

	public function index(){
		
	}

	public function text($data=null){

	}

	public function html($data=null){
		$config['protocol'] 		= 'smtp';
		$config['smtp_host'] 		= 'mail.humanbraincorp.com';
		$config['smtp_port'] 		= 587;
		$config['smtp_user'] 		= 'contactform@humanbraincorp.com';
		$config['smtp_pass'] 		= 'humanbraincorp123';
		$config['smtp_timeout'] 	= 10;
		$config['newline'] 			= "\r\n";
		$config['charset'] 			= 'utf-8';
		$config['wordwrap'] 		= TRUE;
		$config['validate']			= true;
		$config['mailtype'] 		= 'html';
		$this->email->initialize($config);

		$this->email->from('contactform@humanbraincorp.com', 'HBC Contact form');
		$this->email->to('pambeaba@gmail.com');
		$this->email->reply_to('noreply@humanbraincorp.com', 'No reply');
		$this->email->subject('Vous avez reçu un mail du formulaire de contact');
		$this->email->message($this->load->view($data['module'].'/'.$data['view'],$data, true));
		//$this->email->message('test ci3');

		$sent = $this->email->send();


		//This is optional - but good when you're in a testing environment.
		if(isset($sent)){
		   return true;
		}else{
		    return false;
		}
	}

	public function s1html($data=null){
		$config['protocol'] 		= 'smtp';
		$config['smtp_host'] 		= 'mail.humanbraincorp.com';
		$config['smtp_port'] 		= 587;
		$config['smtp_user'] 		= 'contactform@humanbraincorp.com';
		$config['smtp_pass'] 		= 'humanbraincorp123';
		$config['smtp_timeout'] 	= 10;
		$config['newline'] 			= "\r\n";
		$config['charset'] 			= 'utf-8';
		$config['wordwrap'] 		= TRUE;
		$config['validate']			= true;
		$config['mailtype'] 		= 'html';
		$this->email->initialize($config);

		$this->email->from('register@humanbraincorp.com', 'HBC register form');
		$this->email->to($data['to']);
		$this->email->reply_to('noreply@humanbraincorp.com', 'No reply');
		$this->email->subject('Activation de compte');
		$this->email->message($this->load->view($data['module'].'/'.$data['view'],$data, true));
		//$this->email->message('test ci3');

		$sent = $this->email->send();


		//This is optional - but good when you're in a testing environment.
		if(isset($sent)){
		   return true;
		}else{
		    return false;
		}
	}

}

?>