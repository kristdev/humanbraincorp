<!-- La couverture ici -->
<?php echo Modules::run('couverture/show', 0); ?>

<!-- La section creativite -->
<?php echo Modules::run('creativite'); ?>

<!-- La section Blog et neswletter-->
<?php echo Modules::run('blognews'); ?>

<!-- La section Vision et Mission -->
<?php echo Modules::run('visionmission'); ?>

<!-- TESTIMONIAL
================================================== -->
<!--<section>
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-11 col-md-8 col-lg-8">

                <svg width="46px" height="46px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.8 23.9">
                    <path class="fill-primary" d="M661.6,3170.6c.3,0,.5.1.5.3s-1.3,1.3-2.8,2.6-3.7,4.1-3.7,6.8,3.8,5.3,3.8,9-2,5.1-5,5.1-5.7-1.8-5.7-7.3C648.7,3177.9,659,3170.6,661.6,3170.6Zm8,2.5a.4.4,0,0,1,.5.5c0,1.1-3,2.6-3,5.9s4.4,4.2,4.4,7.6a4.2,4.2,0,0,1-4.2,4.2c-3.2,0-5.2-2.5-5.2-5.9C662.1,3178,668.1,3173.1,669.6,3173.1Z" transform="translate(-648.7 -3170.6)"></path>
                </svg>

                
                <div class="testimonial owl-carousel owl-theme mt-5 mt-lg-8">

                    <div class="item">

                        <p class="display-7">Wow! It's an amazing what a theme, I've never seen this type of work before. I'd recommend to buy this theme which want to create amazing website.</p>

                        <div class="d-flex align-items-center justify-content-center">
                            <div class="md-avatar">
                                <img class="img-fluid rounded-circle" src=<?php echo base_url(); ?>myprivate/assets/templates/02/img/avatar/avatar-04.jpg" alt="...">
                            </div>
                            <div class="ml-3 text-left">
                                <h4 class="h5 mb-0">William Warfield</h4>
                                <small class="text-muted">Commercial officer</small>
                            </div>
                        </div>

                    </div>

                    <div class="item">

                        <p class="display-7">Cavistar used for numerous projects ranging from small business websites to large. Template documentation is amazing and well formatted as well.</p>

                        <div class="d-flex align-items-center justify-content-center">
                            <div class="md-avatar">
                                <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/avatar/avatar-05.jpg" alt="...">
                            </div>
                            <div class="ml-3 text-left">
                                <h4 class="h5 mb-0">Ericka Dash</h4>
                                <small class="text-muted">Athletic Director</small>
                            </div>
                        </div>

                    </div>

                    <div class="item">

                        <p class="display-7">It is so flexible and completely customization. I can deliver great quality designs in time. This template is very rich on features and flexible design options.</p>

                        <div class="d-flex align-items-center justify-content-center">
                            <div class="md-avatar">
                                <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/avatar/avatar-06.jpg" alt="...">
                            </div>
                            <div class="ml-3 text-left">
                                <h4 class="h5 mb-0">James Conrad</h4>
                                <small class="text-muted">Instructional Specialist</small>
                            </div>
                        </div>

                    </div>

                </div>
                

            </div>
        </div>
    </div>
</section>-->


<!-- EXTRA
================================================== -->
<!-- <section class="bg-light bottom-shape">
    <div class="container">
        <div class="row align-items-center justify-content-center">

            <div class="col-lg-8 text-center">
                <img class="svg-injector w-40 w-lg-25 mb-4" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/solution.svg" alt="...">
                <h3 class="line-height-base mb-4 font-weight-normal">Build a stunning and beautiful website from scratch with cavistar and bootstrap components.</h3>
                <a href="#!" class="btn btn-warning">Learn More</a>
            </div>

        </div>
    </div>

    
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-dark" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>

</section> -->