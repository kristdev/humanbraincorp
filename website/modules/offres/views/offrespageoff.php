<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-60 bottom-shape" data-jarallax data-speed="0.8" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/offres.jpg">
    <div class="container pt-5 pb-4">

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 text-center">
                <h1 class="display-4 text-white font-weight-bold mb-3">Nos offres</h1>
                <p class="lead text-white-90 mb-5 w-100 w-md-80 mx-auto">
                    Recrutement, offres d'emploi et carrières. Consultez tous nos postes vacants ci-dessous.
                </p>

                <!-- <p class="mb-0 font-weight-bold text-white"><i class="fas fa-map-marker-alt text-warning mr-2 my-1 my-sm-0"></i> Chicago, US, Full Time. <span class="badge badge-pill badge-warning d-inline-block my-1 my-sm-0">$50k - $75k</span></p> -->

            </div>

        </div>

    </div>

    <!-- Bg shape -->
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-white" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>
    <!-- End Bg shape -->

</section>

<!-- EXTRA
================================================== -->
<section>
    <div class="container">
        <div class="row text-center justify-content-center">

            <div class="col-md-11">

                <div class="form-row">

                    <div class="col-md-3 d-none d-lg-block">
                        <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/career1.jpg" class="img-fluid rounded mb-2 transition-hover" alt="career 1">
                        <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/career2.jpg" class="img-fluid rounded mt-2 transition-hover" alt="career 2">
                    </div>
                    <div class="col-sm-6 col-lg-5 mb-5 mb-sm-0">

                        <div class="card h-100 bg-primary rounded transition-hover border-0 d-table py-7 py-sm-0 p-lg-4 p-xl-6">
                            <div class="card-body d-table-cell align-middle">
                                <figure class="mb-4">
                                    <svg width="46px" height="46px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.8 23.9">
                                        <path class="fill-white" d="M661.6,3170.6c.3,0,.5.1.5.3s-1.3,1.3-2.8,2.6-3.7,4.1-3.7,6.8,3.8,5.3,3.8,9-2,5.1-5,5.1-5.7-1.8-5.7-7.3C648.7,3177.9,659,3170.6,661.6,3170.6Zm8,2.5a.4.4,0,0,1,.5.5c0,1.1-3,2.6-3,5.9s4.4,4.2,4.4,7.6a4.2,4.2,0,0,1-4.2,4.2c-3.2,0-5.2-2.5-5.2-5.9C662.1,3178,668.1,3173.1,669.6,3173.1Z" transform="translate(-648.7 -3170.6)"></path>
                                    </svg>
                                </figure>

                                <h3 class="h4 font-weight-normal text-white-95 line-height-base mb-0">
                                    Quelle est votre passion ? Nous recherchons des personnes talentueuses. Choisissez un métier que vous aimez.
                                </h3>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/career3.jpg" class="img-fluid rounded transition-hover" alt="career 3">
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<!-- POSITION
================================================== -->
<section class="border-top border-light">
    <div class="container">

        <div class="row justify-content-center mb-5 mb-md-7">
            <div class="col-md-9 col-lg-6 text-center">

                <!-- Heading -->
                <h3 class="font-weight-normal line-height-base"><span class="emphasis">Notre liste des offres clôturées</span></h3>

            </div>
        </div>

        <!-- Position -->
        <div class="row justify-content-center">
            <div class="col-lg-10">

                <?php 
                    foreach($queryresult as $data):
                ?>

                <a class="card text-dark mb-3 transition-hover" href="<?php echo base_url().'offres/show/'.$data->slug; ?>">
                    <div class="card-body py-3 px-4">
                        <div class="row justify-content-sm-between align-items-sm-center">
                            <div class="col-sm-6 col-xl-7 mb-2 mb-sm-0">
                                <span class="text-secondary"><?php echo $data->societe; ?></span>
                                <br> <?php echo $data->libelle; ?>
                                <br> Clôture dossiers: <?php echo $data->fin_offre; ?>
                            </div>
                            <div class="col-sm-3 mb-2 mb-sm-0 text-secondary">
                                <?php echo $data->localisation; ?> <br> <?php echo $data->contrat; ?>
                            </div>
                            <div class="col-sm-3 col-xl-2 text-primary text-sm-right">
                                Voir détail &rarr;
                            </div>
                        </div>
                    </div>
                </a>

            <?php endforeach; ?>

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <?php echo $links; ?>
                    </ul>
                </nav>

        </div>
        <!-- End Position -->

        <div class="row justify-content-center mt-3 mt-md-4">
            <div class="col-auto">
                <div class="alert bg-warning text-white rounded mb-0">
                   Vous souhaitez visiter nos offres cours? <a href="<?php echo base_url(); ?>offres/" class="font-weight-bold text-white">Cliquez ici!</a>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- BACKGROUND
================================================== -->
<div class="jarallax min-height-lg-50vh d-none d-lg-block py-9 py-lg-11 py-xl-15" data-jarallax data-speed="0.7" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/career4.jpg);">

</div>