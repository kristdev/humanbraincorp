<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-60 bottom-shape" data-jarallax data-speed="0.8" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/team.jpg">
    <div class="container pt-5 pb-4">

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 text-center">
                <h1 class="display-4 text-white font-weight-bold mb-3"><?php if($result) echo $result->libelle;?></h1>
                <!-- <p class="lead text-white-90 mb-5 w-100 w-md-80 mx-auto">
                    Bienvenue dans l'équipe de ceux qui veulent changer, et faire différement
                </p> -->

                <!-- <p class="mb-0 font-weight-bold text-white"><i class="fas fa-map-marker-alt text-warning mr-2 my-1 my-sm-0"></i> Chicago, US, Full Time. <span class="badge badge-pill badge-warning d-inline-block my-1 my-sm-0">$50k - $75k</span></p> -->

            </div>

        </div>

    </div>

    <!-- Bg shape -->
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-white" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>
    <!-- End Bg shape -->

</section>


<!-- CARRER DETAIL
================================================== -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">

                <div class="row align-items-center mb-6 pb-6 border-bottom border-light">

                    <div class="col-6">
                        <a href="<?php echo base_url(); ?>offres"><i class="fas fa-long-arrow-alt-left mr-2"></i>Les offres en cours</a>
                    </div>

                    <div class="col-6 text-right">
                        <a class="btn btn-primary" href="#candidature">Poser sa candidature</a>
                    </div>

                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Société:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->societe; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Contrat:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->contrat; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Localisation:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->localisation; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Fin_offre:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->fin_offre; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Mission:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->mission; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-12">
                        <h3>Profil:</h3>
                        <div class="card bg-light">
                            <div class="card-body p-4 p-lg-6">
                                <div class="card-text">
                                    <?php if($result) echo $result->profil; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>


<!-- FORM
================================================== -->
<section class="bg-light" id="candidature">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">

                <!-- Form -->
                <div class="border border-light bg-white py-6 px-4 px-lg-6 rounded">

                    <h3 class="h2 text-center pb-4 text-primary border-bottom border-light mb-6">Prêt à déposer sa candidature?</h3>

                    <?php 
                        $group = 'candidat';
                        if ($this->ion_auth->in_group($group)):
                    ?>
                    <?php 
                        $userdata = $this->ion_auth->user()->row();
                    ?>
                    <?php 
                        if(isset($success_postulat)):
                    ?>
                    <p class="text-success"><?php echo $success_postulat;?></p>
                    <?php endif; ?>
                    <?php 
                        if(isset($error_postulat)):
                    ?>
                    <p class="text-danger"><?php echo $error_postulat;?></p>
                    <?php endif; ?>
                    <form action="<?php echo base_url(); ?>offres/show/<?php echo $slug; ?>/post" method="post" enctype="multipart/form-data" onclick="">
                        <div class="quform-elements">

                            <div class="row">

                                <!-- Begin Text input element -->
                                <div class="col-md-6">
                                    <div class="quform-element form-group mb-5">
                                        <label for="name">Nom<span class="quform-required">*</span></label>
                                        <div class="quform-input">
                                            <input class="form-control" id="name" type="text" name="name" placeholder="First Name" value="<?php echo $userdata->first_name; ?>" disabled />
                                        </div>
                                    </div>

                                </div>
                                <!-- End Text input element -->

                                <!-- Begin Text input element -->
                                <div class="col-md-6">
                                    <div class="quform-element form-group mb-5">
                                        <label for="last_name">Prénom <span class="quform-required">*</span></label>
                                        <div class="quform-input">
                                            <input class="form-control" id="last_name" type="text" name="last_name" placeholder="Last Name" value="<?php echo $userdata->last_name; ?>" disabled />
                                        </div>
                                    </div>
                                </div>
                                <!-- End Text input element -->

                                <!-- Begin Text input element -->
                                <div class="col-md-6">
                                    <div class="quform-element form-group mb-5">
                                        <label for="email">Email Address <span class="quform-required">*</span></label>
                                        <div class="quform-input">
                                            <input class="form-control" id="email" type="text" name="email" placeholder="name@example.com" value="<?php echo $userdata->email; ?>" disabled />
                                        </div>
                                    </div>
                                </div>
                                <!-- End Text input element -->

                                <!-- Begin Select element -->
                                <div class="col-md-6">
                                    <div class="quform-element form-group mb-5">

                                        <!-- Begin Upload element -->
                                        <div class="quform-input">

                                            <label for="resume">Resume/CV <span class="quform-required">*</span></label>

                                            <div class="">
                                                <select class="form-control" name="cv">
                                                    <?php 
                                                        $where = array(
                                                            'candidat_id'   => $userdata->id
                                                        );
                                                        $querycvs= $this->offrestab->get_allwhereresults('cv',$where);
                                                        $querycvsresult = $querycvs->result();
                                                        var_dump($querycvsresult);
                                                        foreach ($querycvsresult as $key => $value):
                                                    ?>
                                                        <option value="<?php echo $value->id; ?>"><?php echo $value->libelle; ?></option>
                                                    <?php 
                                                        endforeach;
                                                    ?>
                                                </select>
                                            </div>

                                            <p class="quform-description">Vos CVs sont gérés depuis votre gestionaire de <a href="<?php echo base_url(); ?>candidatures">candidatures spontanées</a></p>

                                        </div>
                                        <!-- End Upload element -->

                                    </div>

                                </div>
                                <!-- End Select element -->

                                <!-- Begin Textarea element -->
                                <div class="col-md-12">
                                    <div class="quform-element form-group mb-5">
                                        <label for="message">About me <span class="quform-required">*</span></label>
                                        <div class="quform-input">
                                            <textarea class="form-control" id="message" name="message" rows="6" placeholder="How'd you hear about Cavistar?" disabled="">
                                                <?php echo $userdata->aboutme; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Textarea element -->

                                <!-- Begin Submit button -->
                                <div class="col-md-12">
                                    <div class="quform-submit-inner text-center">
                                        <button class="btn btn-primary btn-wide btn-md mb-0" type="submit"><span>Apply Now</span></button>
                                    </div>
                                    <div class="quform-loading-wrap"><span class="quform-loading"></span></div>
                                </div>
                                <!-- End Submit button -->

                            </div>

                        </div>
                    </form>
                    <?php 
                        else:
                    ?>
                    <div class="text-center">
                        <a class="btn btn-primary" href="<?php echo base_url(); ?>candidatures">Connectez vous</a>
                    </div>
                    <?php
                        endif;
                    ?>

                </div>
                <!-- End Form -->

            </div>
        </div>
    </div>
</section>