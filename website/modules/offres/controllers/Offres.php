<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offres extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->model('Mdl_offres', 'offrestab');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
	}

	public function index(){
		$data['metatitle'] = "Nos offres en cours";
		$data['module'] = "offres";
		$data['view'] = "offrespage";

		$config['base_url'] = base_url().'offres/pageon/';
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['per_page'] = 5;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$where = array(
			'fin_offre >=' => date('y-m-d')
		);
		$limit = $config['per_page'];
		$offset = $start_index;
		$orderby = 'id';
		$set = 'desc';
		
		$query= $this->offrestab->get_allwherebornedresults('offres',$where,$limit,$offset,$orderby,$set);
		$config['total_rows'] = $query->num_rows();
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['attributes'] = array('class' => 'page-link');

		
		$queryresult = $query->result();



		$this->pagination->initialize($config);

		$links = $this->pagination->create_links();
		$data['links'] = $links;

		$data['queryresult'] = $queryresult;
		echo Modules::run('template/home', $data);
	}

	public function pageon(){
		$data['metatitle'] = "Nos offres en cours";
		$data['module'] = "offres";
		$data['view'] = "offrespage";

		$config['base_url'] = base_url().'offres/pageon/';
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['per_page'] = 5;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$where = array(
			'fin_offre >=' => date('y-m-d')
		);
		$limit = $config['per_page'];
		$offset = $start_index;
		$orderby = 'id';
		$set = 'desc';
		
		$query= $this->offrestab->get_allwherebornedresults('offres',$where,$limit,$offset,$orderby,$set);
		$config['total_rows'] = $query->num_rows();
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['attributes'] = array('class' => 'page-link');

		
		$queryresult = $query->result();



		$this->pagination->initialize($config);

		$links = $this->pagination->create_links();
		$data['links'] = $links;

		$data['queryresult'] = $queryresult;
		echo Modules::run('template/home', $data);
	}

	public function off(){
		$data['metatitle'] = "Nos offres clôturées";
		$data['module'] = "offres";
		$data['view'] = "offrespageoff";
		$data['test'] = 'test';

		$config['base_url'] = base_url().'offres/pageoff/';
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['per_page'] = 5;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$where = array(
			'fin_offre <' => date('y-m-d')
		);
		$limit = $config['per_page'];
		$offset = $start_index;
		$orderby = 'id';
		$set = 'desc';
		
		$query= $this->offrestab->get_allwherebornedresults('offres',$where,$limit,$offset,$orderby,$set);
		$config['total_rows'] = $query->num_rows();
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['attributes'] = array('class' => 'page-link');

		
		$queryresult = $query->result();



		$this->pagination->initialize($config);

		$links = $this->pagination->create_links();
		$data['links'] = $links;

		$data['queryresult'] = $queryresult;
		echo Modules::run('template/home', $data);
	}

	public function pageoff(){
		$data['metatitle'] = "Nos offres clôturées";
		$data['module'] = "offres";
		$data['view'] = "offrespage";
		$data['test'] = 'test';

		$config['base_url'] = base_url().'offres/pageoff/';
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['per_page'] = 5;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$where = array(
			'fin_offre <' => date('y-m-d')
		);
		$limit = $config['per_page'];
		$offset = $start_index;
		$orderby = 'id';
		$set = 'desc';
		
		$query= $this->offrestab->get_allwherebornedresults('offres',$where,$limit,$offset,$orderby,$set);
		$config['total_rows'] = $query->num_rows();
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['attributes'] = array('class' => 'page-link');

		
		$queryresult = $query->result();



		$this->pagination->initialize($config);

		$links = $this->pagination->create_links();
		$data['links'] = $links;

		$data['queryresult'] = $queryresult;
		echo Modules::run('template/home', $data);
	}

	public function show($slug=null, $key=null){
		if($slug == null){
			$this->index();
		}else{
			if($key != 'post'){
				$table = 'offres';
				$where = array(
					'slug' => $slug
				);
				$query = $this->offrestab->get_allwhereresults($table,$where,$orderby=null,$set=null);
				$result = $query->row();

				if(empty($result) != true){					
					$data['metatitle'] = "Offre - ".$result->libelle;
					$data['module'] = "offres";
					$data['view'] = "offrepage";
					$data['result'] = $result;
					$data['slug'] = $slug;
					echo Modules::run('template/home', $data);
				}else{
					$this->index();
				}

			}else{
				$table = 'offres';
				$where = array(
					'slug' => $slug
				);
				$query = $this->offrestab->get_allwhereresults($table,$where,$orderby=null,$set=null);
				$result = $query->row();
				$datapost = $this->input->post();
				if(empty($datapost) == true){
					$this->show();
				}else{
					$cv = $datapost['cv'];
					$where = array(
						'offres.slug' => $slug,
						'postulats.cv_id' => $cv
					);
					$querypostulat = $this	->db
											->select('*')
											->from('postulats')
											->join('offres', 'postulats.offre_id = offres.id')
											->join('cv', 'postulats.cv_id = cv.id')
											->join('users', 'cv.candidat_id = users.id')
											->where($where);
					$querypostulat = $this->db->get();
					$postulatsresult = $querypostulat->result();
					if(empty($postulatsresult) == true){
						$table = 'offres';
						$where = array(
							'slug' => $slug
						);
						$query = $this->offrestab->get_allwhereresults($table,$where,$orderby=null,$set=null);
						$result = $query->row();
						$offre_id = $result->id;
						$datainsert = array(
							'offre_id' 		=> $offre_id,
							'cv_id'			=> $cv,
							'candidat_id'	=>  $this->ion_auth->user()->row()->id
						);
						$queryinsert = $this->offrestab->insert_somedata('postulats',$datainsert);
						if($queryinsert){
							$data['success_postulat'] = 'Vous venez de postuler à cette offre, vous serez contactés lors du processus de validation';
						}else{
							$data['error_postulat'] = 'Une erreur est survenue, veuillez recommencer la procédure';
						}
						$data['module'] = "offres";
						$data['view'] = "offrepage";
						$data['result'] = $result;
						$data['slug'] = $slug;
						echo Modules::run('template/home', $data);
					}else{
						$data['error_postulat'] = 'Vous ne pouvez pas postuler à cette offre avec le même cv';
						$data['module'] = "offres";
						$data['view'] = "offrepage";
						$data['result'] = $result;
						$data['slug'] = $slug;
						echo Modules::run('template/home', $data);
					}
				}
			}
		}
	}

}

?>