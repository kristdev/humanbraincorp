<!-- HEADER
================================================== -->
<header>

    <div class="container">

        <nav class="navbar navbar-expand-lg navbar-light bg-white py-3">

            <!-- Brand -->
            <a class="navbar-brand mr-lg-5" href="index.html">
                <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/logos/logohbc.svg" alt="HBC Logo">
            </a>
            <!-- End Brand -->

            <!-- Toggler -->
            <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- End Toggler -->

            <!-- Collaps -->
            <div id="navbarContent" class="collapse navbar-collapse">
                <!-- Navigation -->
                <ul class="navbar-nav align-items-lg-center ml-auto">

                    <li class="nav-item <?php if($this->uri->segment(1, 0) === 0) echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item <?php if($this->uri->segment(1, 0) === 'about') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>about" class="nav-link">A propos</a>
                    </li>
                    <?php  
                    $expertisesquery = Modules::run('expertises/list');
                    $expertises = $expertisesquery->result();
                    ?>
                    <li class="nav-item <?php if ($expertisesquery->num_rows() != 0) echo 'dropdown';  ?> <?php if($this->uri->segment(1, 0) === 'expertises') echo 'active'; ?>">
                        <a href="<?php echo base_url().'expertises/'; ?>" class="nav-link <?php if ($expertisesquery->num_rows() != 0) echo 'dropdown-toggle';  ?>">Expertise</a>
                        <ul class="dropdown-menu shadow">
                        <?php 
                            foreach($expertises as $expertise){
                            ?>    
                            <li class="dropdown-submenu">
                                <a href="<?php echo base_url().'expertises/show/'.$expertise->slug; ?>" class="dropdown-item"><?php echo $expertise->libelle; ?></a>
                            </li>
                        <?php 
                            }
                        ?>

                        </ul>
                    </li>
                    <?php  
                    $catblogquery = Modules::run('blog/listcategories');
                    $categories = $catblogquery->result();
                    ?>
                    <li class="nav-item dropdown <?php if($this->uri->segment(1, 0) === 'blog') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>blog" class="nav-link dropdown-toggle">Notre blog</a>
                        <ul class="dropdown-menu shadow">
                        <?php 
                            foreach($categories as $category){
                            ?>    
                            <li class="dropdown-submenu">
                                <a href="<?php echo base_url().'blog/category/'.$category->slug; ?>" class="dropdown-item"><?php echo $category->libelle; ?></a>
                            </li>
                        <?php 
                            }
                        ?>
                        </ul>
                    </li>

                    <li class="nav-item <?php if($this->uri->segment(1, 0) === 'contact') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>contact" class="nav-link">Contact</a>
                    </li>

                    <li class="nav-item mr-0 dropdown">
                        <a data-toggle="dropdown" class="btn btn-sm btn-primary ml-auto dropdown-toggle" target="_blank" href="#!">Carrières</a>
                        <ul class="dropdown-menu shadow pt-2 pb-0 py-lg-3">
                            <li>
                                <a href="<?php echo base_url(); ?>offres" class="dropdown-item">
                                    <div class="d-flex border-bottom border-light pb-3">
                                        <img class="feather display-6" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/svg/file-text.svg" alt="...">
                                        <div class="ml-3">
                                            <h6 class="mb-0">Offres d'emploi</h6>
                                            <!-- <p class="small mb-0">Customizing and building</p> -->
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>candidatures" class="dropdown-item">
                                    <div class="d-flex border-bottom border-light pt-2 pb-3 pt-lg-1">
                                        <img class="feather display-6" src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/svg/grid.svg" alt="...">
                                        <div class="ml-3">
                                            <h6 class="mb-0">Candidatures spontanées</h6>
                                            <p class="small mb-0">Postulez pour nos offres d'emplois</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>

                <!-- <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                    
                </ul> -->
                <!-- End Navigation -->
            </div>
            <!-- End Collaps -->

        </nav>

    </div>

</header>