<!DOCTYPE html>
<html lang="fr">

<head>

    <!-- Title  -->
    <title>HBC | <?php echo $metatitle ?? null; ?></title>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>myprivate/assets/templates/02/img/hbfavicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Libs CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jarallax/dist/jarallax.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/owl-carousel/dist/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/owl-carousel/dist/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/quform/css/base.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/easy-responsive-tabs/css/easy-responsive-tabs.css">

    <!-- Theme CSS -->
    <link href="<?php echo base_url(); ?>myprivate/assets/templates/02/css/theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/css/customcss.css">

</head>

<body>

    <!-- PAGE LOADING
    ================================================== -->
    <div id="preloader">
        <div class="row loader">
            <div class="loader-icon"></div>
        </div>
    </div>

    <!-- MAIN WRAPPER
    ================================================== -->
    <div class="main-wrapper">

        <!-- header ici -->
        <?php $this->load->view('partials/header'); ?>

        <?php 
            if (isset($view) and isset($module)) {
                $this->load->view($module.'/'.$view);
            }
        ?>

        <!-- FOOTER
        ================================================== -->
        <footer class="bg-dark pt-8 pt-lg-10">
            <div class="container">
                <div class="row pb-8 pb-lg-10">
                    <div class="col-sm-6 col-lg-4 mb-5 mb-lg-0">
                        <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/logos/logohbc.svg" class="mb-4 w-40 w-sm-45 w-md-35 w-lg-40" alt="...">

                        <!-- <h5 class="h6 font-weight-normal text-white-90 line-height-base w-85">
                            <span class="text-warning">Cavistar</span> gives you all elements which are necessary for your design goals.
                        </h5> -->

                        <ul class="list-unstyled p-0 m-0">
                            <!-- <li class="pb-2 text-white-60"><span>Bonamouss</span></li> -->
                            <li class="pb-2 text-white-60"><span>(+237) 653 689 588 / 691 305 258</span></li>
                            <li class="text-white-60"><span>ninangougue@humanbraincorp.com</span></li>
                            <li class="text-white-60"><span>stephanefoka@humanbraincorp.com</span></li>
                        </ul>
                    </div>

                    <div class="col-sm-6 col-lg-2 mb-5 mb-lg-0">
                        <h3 class="h4 font-weight-normal mb-3 mb-lg-4 text-white">Services</h3>
                        <?php  
                        $expertisesquery = Modules::run('expertises/list');
                        $expertises = $expertisesquery->result();
                        ?>
                        <ul class="list-unstyled p-0 m-0">
                            <?php 
                                foreach($expertises as $expertise){
                            ?> 
                            <li class="pb-2"><a href="<?php echo base_url().'expertises/show/'.$expertise->slug; ?>" class="text-white-60 hover-white"><?php echo $expertise->libelle; ?></a></li>
                            <?php 
                                }
                            ?>
                        </ul>
                    </div>

                    <div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
                        <div class="pl-0 pl-lg-8">
                            <h3 class="h4 font-weight-normal mb-3 mb-lg-4 text-white">À propos</h3>
                            <ul class="list-unstyled p-0 m-0">
                                <li class="pb-2"><a href="<?php echo base_url(); ?>about" class="text-white-60 hover-white">About</a></li>
                                <li class="py-2"><a href="<?php echo base_url(); ?>contact" class="text-white-60 hover-white">Nous écrire</a></li>
                            </ul>
                            <div>
                                <ul class="list-unstyled social-icon3 p-0 m-0">
                                    <li><a href="#!"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#!"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#!"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#!"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <h3 class="h4 font-weight-normal mb-3 mb-lg-4 text-white">Restez informés</h3>
                        <div class="mb-4">

                            <!-- start form here -->

                            <form class="quform" action="http://localhost/myprivatehbc/assets/templates/02/libs/quform/newsletter-one.php" method="post" enctype="multipart/form-data" onclick="">

                                <div class="quform-elements">

                                    <div class="row">

                                        <!-- Begin Text input element -->
                                        <div class="col-md-12">
                                            <div class="quform-element form-group">
                                                <div class="quform-input">
                                                    <input class="form-control" id="nom" type="text" name="nom" placeholder="Enter your name" />
                                                    <br>
                                                    <input class="form-control" id="email_address" type="text" name="email_address" placeholder="Enter your email" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Text input element -->

                                        <!-- Begin Submit button -->
                                        <div class="col-md-12">
                                            <div class="quform-submit-inner">
                                                <button class="btn btn-primary" type="submit"><span>Subscribe</span></button>
                                            </div>
                                            <div class="quform-loading-wrap text-left"><span class="quform-loading"></span></div>
                                        </div>
                                        <!-- End Submit button -->

                                    </div>

                                </div>

                            </form>

                            <!-- end form here -->

                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center border-top py-4 border-light-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-white-95 mb-0">&copy; Copyright HBC 2020. Designed and Builded by kristdev .</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

    <!-- SCROLL TO TOP
    ================================================== -->
    <a href="#!" class="scroll-to-top"><i class="fas fa-angle-up" aria-hidden="true"></i></a>

    <!-- ALL JAVASCRIPT
    ================================================== -->

    <!-- Global JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jquery/dist/jquery-migrate.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Libs JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/svg-injector/dist/svg-injector.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/owl-carousel/dist/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jarallax/dist/jarallax.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/quform/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/quform/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/easy-responsive-tabs/js/easyResponsiveTabs.js"></script>
    <!--Mediawrapper-->
    <script src="<?php echo base_url(); ?>myprivate/assets/jquery.mediaWrapper.js"></script>

    <!-- Theme JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/js/theme.js"></script>

    <script>
        $(document).ready(function() {
            // Iframe du grocery crud
                $('iframe').mediaWrapper({
                    intrinsic:  true
                });
        });
    </script>

</body>

</html>
