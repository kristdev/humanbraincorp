<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Title  -->
    <title>HBC | <?php echo $metatitle ?? null; ?></title>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>myprivate/assets/templates/02/img/hbfavicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Libs CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jarallax/dist/jarallax.css">

    <!-- Theme CSS -->
    <link href="<?php echo base_url(); ?>myprivate/assets/templates/02/css/theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>myprivate/assets/templates/02/css/customcss.css">

</head>

<body>

    <!-- PAGE LOADING
    ================================================== -->
    <div id="preloader">
        <div class="row loader">
            <div class="loader-icon"></div>
        </div>
    </div>

    <!-- MAIN WRAPPER
    ================================================== -->
    <div class="main-wrapper">

        <!-- LOGIN CONTENT
        ================================================== -->
        <div class="d-flex align-items-center position-relative min-vh-100">

            <!-- Left content -->
            <div class="jarallax overlay overlay-primary overlay-70 col-lg-5 col-xl-4 d-none d-lg-flex align-items-center h-100vh px-0" data-jarallax data-speed="0.9" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/vericla-cover.jpg);">

                <div class="w-100 p-5 text-center">


                    <div class="position-absolute right-0 top-0 left-0 p-5 mx-auto w-50">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/logoblanc.png" width="200" class="w-lg-90 w-xl-80" alt="..."></a>
                    </div>

                    <div class="position-relative">
                        <h1 class="text-white font-weight-normal">Joignez notre communauté</h1>
                        <p class="text-white-90 mb-0 w-85 w-xl-70 mx-auto">
                            Consultez les derniers recrutements,  postulez à nos offres d'emploi et façonnez votre carrière
                        </p>
                    </div>

                    <div class="position-absolute right-0 bottom-0 left-0 text-center text-white p-5">
                        <div class="row">
                            <div class="col-6">
                                <p class="mb-0 mt-1">&copy; <span class="text-white-85"> Humanbraincorp</span></p>
                            </div>
                            <div class="col-6">
                                <ul class="list-inline-item mb-0">
                                    <li class="list-inline-item"><a href="#!" class="text-white-85 hover-white">Privacy</a></li>
                                    <li class="list-inline-item"><a href="#!" class="text-white-85 hover-white">Terms</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Left content -->

            <!-- Login form -->
            <div class="container">
                <div class="row justify-content-center justify-content-lg-start">
                    <div class="col-md-8 col-lg-7 col-xl-5 offset-lg-2 offset-xl-3 my-5">

                        <div class="mb-6">
                            <h2 class="font-weight-normal mb-0">Bienvenue sur <span class="text-primary">H.B.C</span></h2>
                            <p class="mb-0"><?php if(isset($metatitle)) echo $metatitle;?></p>
                        </div>

                        <?php 
                            if(isset($module) and isset($view)){
                                $this->load->view($module.'/'.$view);
                            }
                        ?>

                    </div>
                </div>
            </div>
            <!-- End Login form -->

        </div>

    </div>

    <!-- ALL JAVASCRIPT
    ================================================== -->

    <!-- Global JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jquery/dist/jquery-migrate.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Libs JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/svg-injector/dist/svg-injector.min.js"></script>
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/libs/jarallax/dist/jarallax.min.js"></script>

    <!-- Theme JS -->
    <script src="<?php echo base_url(); ?>myprivate/assets/templates/02/js/theme.js"></script>

</body>

</html>