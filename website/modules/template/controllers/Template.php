<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->helper('url');
	}

	public function home($data=null){
		$this->load->view('layouts/@home', $data);
	}

	public function pub($data=null){
		$this->load->view('layouts/@public', $data);
	}
	
	public function error404(){
		$this->load->view('_404');
	}

	public function error500(){
		$this->load->view('_500');
	}

	public function candidatureslr($data=null){
		$this->load->view('layouts/@candidatureslogin', $data);
	}

}

?>