<div class="text-warning">
    <?php echo validation_errors(); ?>
</div>
<?php 
    if(isset($error_register)):
?>
<div class="alert alert-danger" role="alert">
  <?php echo $error_register; ?>
</div>
<?php 
    endif;
?>

<?php 
    if(isset($success_register)):
?>
<div class="alert alert-success" role="alert">
  <?php echo $success_register; ?>
</div>
<?php 
    endif;
?>
<form action="<?php echo base_url().'/candidatures/register'; ?>" method="post">

    <div class="row">

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username">
            </div>

        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Email Address</label>
                <input type="text" class="form-control" name="email" placeholder="Your email address">
            </div>

        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Your password">
            </div>

        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="repassword" placeholder="Your confirm password">
            </div>

        </div>
        <!-- End Input -->

    </div>

    <div class="row mt-2">
        <div class="col-md-12 center">
            <button type="submit" class="btn btn-primary btn-block mt-4 mb-0">Register</button>
        </div>
    </div>

    <div class="text-center text-small mt-4">
        <span>Already have an account? <a href="<?php echo base_url(); ?>candidatures/login">Login</a></span>
    </div>
</form>