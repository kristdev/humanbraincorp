<form>
    <div class="row">
        <!-- Input -->
        <div class="col-12 mb-2">
            <div class="form-group">
                <label>Email Address</label>
                <input type="text" class="form-control" name="email" placeholder="Your email address">
            </div>
        </div>
        <!-- End Input -->
    </div>
    <div class="row mt-2">
        <div class="col-md-12 center">
            <button type="submit" class="btn btn-primary btn-block mb-0">Request Reset Link</button>
        </div>
    </div>
    <div class="text-center text-small mt-4">
        <span>Remember password? <a href="<?php echo base_url(); ?>candidatures/login">Back to Login</a></span>
    </div>
</form>