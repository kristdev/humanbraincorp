<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-70" data-jarallax data-speed="0.7" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/candidatures.jpg);">

    <div class="container">
        <div class="row justify-content-center text-center">

            <div class="col-lg-10 col-xl-8">

                <h1 style="color: white;">Espace candidat</h1>
                <p class="lead w-100 w-lg-80 mx-auto mb-0">Editez votre profil, votre mot de passe, ajoutez des CVs et consultez candidatures</p>

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <?php 
                    if(isset($error_update)):
                ?>
                <div class="alert alert-danger" role="alert">
                  <?php echo $error_update; ?>
                </div>
                <?php 
                    endif;
                ?>
                <div class="text-right">
                    <a href="<?php echo base_url(); ?>candidatures/logout" class="btn btn-danger"><i class="fa fa-stop"></i> Se déconnecter</a>
                </div>
                <div class="horizontaltab tab-style-one mt-5">
                    <ul class="resp-tabs-list hor_1">
                        <li><span>Profile</span></li>
                        <li><span>Mdification du mot de passe</span></li>
                        <li><span>Vos CVs</span></li>
                        <li><span>Vos candidatures</span></li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>

                            <div class="py-5 py-lg-7">

                                <!-- Form -->
                                <form action="<?php echo base_url(); ?>candidatures/updateaccount" method="post">
                                    <div class="mb-5">
                                        <h3 class="mb-2 h4">Détails du compte</h3>
                                        <p class="display-12 text-muted mb-0">Your information is always kept private, It's never been accessed by others. <a href="#!">View our Privacy Policy</a></p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="firstname" placeholder="Your first name" value="<?php echo $userdata->first_name ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="lastname" placeholder="Your last name" value="<?php echo $userdata->last_name ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Username</label>
                                                <input type="text" class="form-control" name="username" placeholder="Your username" value="<?php echo $userdata->username ?? null; ?>" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Email address</label>
                                                <input type="email" class="form-control" name="email" placeholder="examples@gmail.com" value="<?php echo $userdata->email ?? null; ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-5">
                                        <h3 class="mb-2 h4">Profile Information</h3>
                                        <p class="display-12 text-muted mb-0">About Me and Country information will show on your public profile. <a href="#!">View your Public Profile</a></p>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="city" placeholder="Your city name" value="<?php echo $userdata->city ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Country</label>
                                                <input type="text" class="form-control" name="country" placeholder="Your country name" value="<?php echo $userdata->country ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Phone</label>
                                                <input type="text" class="form-control" name="phone" placeholder="237 6xxxxxxxx" value="<?php echo $userdata->phone ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-5">
                                                <label>Website</label>
                                                <input type="text" class="form-control" name="website" placeholder="Your website here" value="<?php echo $userdata->website ?? null; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group mb-5">
                                                <label>About Me</label>
                                                <textarea id="about" rows="6" class="form-control" placeholder="Tell us a few words about yourself" name="aboutme">
                                                    <?php echo $userdata->aboutme ?? null; ?>
                                                </textarea>
                                                <!-- <small class="text-muted">You can @mention other organizations link.</small> -->
                                            </div>
                                        </div>

                                    </div>


                                        <div class="col-md-12">
                                            <!-- Buttons -->
                                            <button type="submit" class="btn btn-primary mr-2">Save Changes</button>
                                            <button type="reset" class="btn btn-light">Cancel</button>
                                            <!-- End Buttons -->
                                        </div>

                                </form>
                                <!-- End Form -->

                            </div>

                        </div>
                        <div>
                            <div class="pt-0 pb-5 py-lg-7">

                                <div class="mb-5">
                                    <h3 class="mb-2 h4">Change Password</h3>
                                    <p class="display-12 text-muted mb-0">Vous recevrez un mail de reinitialiation dans votre boîte mail </p>
                                </div>

                                <!-- Form -->
                                <form>

                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <!-- Buttons -->
                                            <a href="#!" class="btn btn-light">Je veux changer mon mot de passe</a>
                                            <!-- End Buttons -->
                                        </div>
                                    </div>


                                </form>
                                <!-- End Form -->

                            </div>

                        </div>
                        <div>

                            <div class="pt-0 pt-lg-7">

                                <div class="mb-4">
                                    <h3 class="mb-2 h4">Gestion des CVs</h3>
                                    <p class="display-12 text-muted mb-0">Ajoutez, éditez vos CVs</p>
                                </div>

                                <iframe src="<?php echo base_url(); ?>gc_cv" frameborder="0"></iframe>

                            </div>

                        </div>
                        <div>

                            <div class="pt-0 pt-lg-7">

                                <div class="mb-5">
                                    <h3 class="mb-2 h4">Les offres</h3>
                                    <p class="display-12 text-muted mb-0">Liste des offres sollicitées</p>
                                </div>

                                <iframe src="<?php echo base_url(); ?>gc_postulats" frameborder="0"></iframe>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>