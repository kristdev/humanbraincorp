<div class="text-warning">
    <?php echo validation_errors(); ?>
</div>
<?php 
    if(isset($error_login)):
?>
<div class="alert alert-danger" role="alert">
  <?php echo $error_login; ?>
</div>
<?php 
    endif;
?>

<?php 
    if(isset($success_confirmaccount)):
?>
<div class="alert alert-sucess" role="alert">
  <?php echo $success_confirmaccount; ?>
</div>
<?php 
    endif;
?>

<?php 
    if(isset($error_confirmaccount)):
?>
<div class="alert alert-danger" role="alert">
  <?php echo $error_confirmaccount; ?>
</div>
<?php 
    endif;
?>

<form action="<?php echo base_url().'/candidatures/login'; ?>" method="post">
    <div class="row">

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email">
            </div>

        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="col-12 mb-2">

            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Your password">
            </div>

        </div>
        <!-- End Input -->

    </div>

    <div class="row mt-2">
        <div class="col-sm-7 mb-2 mb-sm-0">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="login-remember" name="remember">
                <label class="custom-control-label" for="login-remember">Keep me signed in</label>
            </div>
        </div>
        <div class="col-sm-5 text-left text-sm-right">
            <a href="<?php echo base_url(); ?>candidatures/resetpassword" class="m-link-muted small">Forgot password?</a>
        </div>

        <div class="col-md-12 center">
            <button type="submit" class="btn btn-primary btn-block mt-4 mb-2">Sign In</button>
        </div>
    </div>

    <div class="text-center text-small mt-4">
        <span>Don't have an account yet? <a href="<?php echo base_url(); ?>candidatures/register">Register</a></span>
    </div>

</form>