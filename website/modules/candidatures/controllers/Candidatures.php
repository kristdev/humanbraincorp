<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Candidatures extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
		$this->load->database();
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}

	public function index(){
		$group = 'candidat';
		if ($this->ion_auth->in_group($group)){
			redirect(base_url().'candidatures/account', 'refresh');
		}else{
			$this->login();
		}
	}


	public function login(){
		$group = 'candidat';
		if ($this->ion_auth->in_group($group)){
			redirect(base_url().'candidatures/account', 'refresh');
		}else{
			$data['metatitle'] = "Human Brain Corporation login";
			$data['module'] = "candidatures";
			$data['view'] = "candidaturelogin";

			if($this->input->post() !== null){
				$identity = $this->input->post('email');
				$password =  $this->input->post('password');
				$remember = $this->input->post('remember'); // remember the user

				$this->form_validation->set_rules('email', 'email', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');

				if ($this->form_validation->run() == FALSE){
                        echo Modules::run('template/candidatureslr', $data);
                }
                else{
					if($this->ion_auth->login($identity, $password, $remember) == true){
						redirect(base_url().'candidatures/account');
					}else{
						$data['error_login'] = "Username ou password incorrects, veuillez reprendre";
						echo Modules::run('template/candidatureslr', $data);
					}
            	}
			}else{
				echo Modules::run('template/candidatureslr', $data);
			}

		}
	}


	public function logout(){
		$this->ion_auth->logout();
		sleep(2);
		redirect(base_url().'candidatures/', 'refresh');
	}


	public function confirmpassword(){
		echo 'confirm';
	}

	public function resetpassword(){
		$group = 'candidat';
		if ($this->ion_auth->in_group($group)){

		}else{
			$data['metatitle'] = "Human Brain Corporation User password reset";
			$data['module'] = "candidatures";
			$data['view'] = "candidatureresetpassword";
			echo Modules::run('template/candidatureslr', $data);
		}
	}

	public function register(){
		$group = array('2');
		if ($this->ion_auth->in_group($group)){
			header("Location: ".base_url().'candidatures/account');
		}else{
			$data['metatitle'] = "Human Brain Corporation user registration";
			$data['module'] = "candidatures";
			$data['view'] = "candidatureregister";

			if($this->input->post() !== null){
				$username= $this->input->post('username');
				$email = $this->input->post('email');
				$password =  $this->input->post('password');
				$repassword =  $this->input->post('repassword');
				$additional_data = array(
					'active' => 0
				);

				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[12]|is_unique[users.username]');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|alpha_dash');
				$this->form_validation->set_rules('repassword', 'RePassword', 'matches[password]');

				if ($this->form_validation->run() == FALSE){
                        echo Modules::run('template/candidatureslr', $data);
                }
                else{
                	$registerid = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
					if($registerid !== false){
						$userdata = $this->ion_auth->user($registerid['id'])->row();
						//var_dump($userdata);
						$id = $userdata->id;
						$activation = $registerid['activation'];

						$datamail['username'] = $username;
						$datamail['id'] = $id;
						$datamail['username'] = $username;
						$datamail['activation'] = $activation;
						$datamail['to'] = $email;
						$datamail['module'] = "candidatures";
						$datamail['view'] = "mailregister";

						$this->load->module('sendmail', $datamail);

						if($this->sendmail->s1html($datamail) == true){
							$data['success_register'] = "Vous allez recevoir un mail d'activation, bien vouloir ouvrir votre boîte mail";
							$data['metatitle'] = "Human Brain Corporation user registration";
							$data['module'] = "candidatures";
							$data['view'] = "candidatureregister";
							
							echo Modules::run('template/candidatureslr', $data);
						}else{
							$data['error_register'] = "une erreur est survenue, veuillez recommencer l'opération!";
							$data['metatitle'] = "Human Brain Corporation user registration";
							$data['module'] = "candidatures";
							$data['view'] = "candidatureregister";
							echo Modules::run('template/candidatureslr', $data);
						}				
					}else{
						$data['error_register'] = "Une erreur est survenue, veuillez recommencer";
						echo Modules::run('template/candidatureslr', $data);
					}
            	}

			}else{
				echo Modules::run('template/candidatureslr', $data);
			}
		}
	}

	public function confirmaccount($id,$activation){
		if(isset($id) and isset($activation)){
			$activation = $this->ion_auth->activate($id, $activation);
			if($activation == true){
				$data['metatitle'] = "Human Brain Corporation login";
				$data['module'] = "candidatures";
				$data['view'] = "candidaturelogin";
				$data['success_confirmaccount'] = "Votre compte est actif, vous pouvez vous logger";
				echo Modules::run('template/candidatureslr', $data);
			}else{
				$data['metatitle'] = "Human Brain Corporation login";
				$data['module'] = "candidatures";
				$data['view'] = "candidaturelogin";
				$data['error_confirmaccount'] = "Une erreur est survenue pendant l'activation de votre compte, vueillez contacter l'administrateur";
				echo Modules::run('template/candidatureslr', $data);
			}
		}else{
			$this->register();
		}
	}

	public function account(){
		$group = 'candidat';
		if ($this->ion_auth->in_group($group)){
			$data['metatitle'] = "Human Brain Corporation user account";
			$data['module'] = "candidatures";
			$data['view'] = "candidatureaccount";
			$data['userdata'] = $this->ion_auth->user()->row();
			echo Modules::run('template/home', $data);

		}else{
			sleep(1);
			redirect(base_url().'/candidatures', 'refresh');
		}
	}

	public function post_candidature($offre,$cv){
		
	}

	public function updateaccount(){
		$group = 'candidat';
		if ($this->ion_auth->in_group($group)){
			if(!empty($this->input->post())){
				$datareceived = $this->input->post();
				$datacleaned = $this->security->xss_clean($datareceived);
				
				$data['first_name'] = $datacleaned['firstname'];
				$data['last_name'] = $datacleaned['lastname'];
				$data['city'] = $datacleaned['city'];
				$data['country'] = $datacleaned['country'];
				$data['phone'] = $datacleaned['phone'];
				$data['website'] = $datacleaned['website'];
				$data['aboutme'] = $datacleaned['aboutme'];
				$id = $this->ion_auth->user()->row()->id;
				if($this->ion_auth->update($id, $data)){
					redirect(base_url().'/candidatures/account', 'refresh');
				}else{
					$error_update = "Une erreur est survenue, vueillez recommencer";
					$data['metatitle'] = "Human Brain Corporation user account";
					$data['module'] = "candidatures";
					$data['view'] = "candidatureaccount";
					$data['userdata'] = $this->ion_auth->user()->row();
					echo Modules::run('template/home', $data);
				}

			}else{
				redirect(base_url().'/candidatures/account', 'refresh');
			}
		}else{
			redirect(base_url().'/candidatures/login', 'refresh');
		}
	}

}

?>