<!-- BANNER
================================================== -->
<!-- <section class="bg-primary py-0">
    <div class="container">
        <div class="row align-items-center justify-content-center">

            
            <div class="col-xl-8">
                <div class="text-center py-10 py-md-12 py-lg-15">

                    Banner headline text
                    <h1 class="text-white display-4 mb-0">
                        Human Brain Corporation
                    </h1>
                    

                </div>
            </div>
            

        </div>
    </div>

</section> -->

<!-- BANNER
================================================== -->
<section class="jarallax overlay overlay-black overlay-60 bottom-shape" data-jarallax data-speed="0.8" style="background-image: url(<?php echo base_url(); ?>myprivate/assets/templates/02/img/banner/about1.jpg);">
    <div class="container pt-5 pb-4">

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 text-center">
                <h1 class="display-4 text-white font-weight-bold mb-3">Human Brain Corporation</h1>
                <!--<p class="lead text-white-90 mb-5 w-100 w-md-80 mx-auto">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto dolor inventore explicabo, error, cupiditate perspiciatis esse recusandae doloremque porro et, distinctio optio praesentium quod eveniet quas tempora magni soluta similique.
                </p>-->

                <!--<p class="mb-0 font-weight-bold text-white"><i class="fas fa-map-marker-alt text-warning mr-2 my-1 my-sm-0"></i> Chicago, US, Full Time. <span class="badge badge-pill badge-warning d-inline-block my-1 my-sm-0">$50k - $75k</span></p>-->

            </div>

        </div>

    </div>

    <!-- Bg shape -->
    <div class="bg-round-shape">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 115" preserveAspectRatio="none meet" class="ie-height-115">
            <path class="fill-none" d="M-1,0S447.9,95.6,960,95.6c0,0,432.5,9.8,959-95.6Z" transform="translate(1)"></path>
            <path class="fill-white" d="M-1,130V0S521.4,101.6,960,95.6c0,0,440,5.3,959-95.6V130Z" transform="translate(1)"></path>
        </svg>
    </div>
    <!-- End Bg shape -->

</section>

<!-- ABOUT
================================================== -->
<section>
    <div class="container">
        <div class="row align-items-center justify-content-center">

            <div class="col-lg-5 mb-7 mb-lg-0">

                <h2 class="font-weight-normal line-height-base">Qui sommes nous?</h2>
                <p class="w-90 lead">
                    Un Cabinet de consulting en ressources humaines spécialisé dans l’accompagnement personnalisé des individus, des managers, et décideurs d’entreprises dans la mise en œuvre de leur stratégie.
                </p>
                <p class="w-90 lead">
                    Nous proposons des sessions coaching et Formation  qui prennent en compte les évolutions de leurs activités et des compétences de leurs équipes.
                </p>

            </div>

            <div class="col-lg-6 offset-lg-1 text-center">
                <div class="row no-gutters">
                    <div class="col-5 pr-2 align-self-end mb-3">
                        <a href="#!" class="position-relative d-block">
                            <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/manyvalues.jpg" class="rounded" alt="...">
                        </a>
                    </div>
                    <div class="col-7 pl-2 mb-3">
                        <a href="#!" class="position-relative d-block">
                            <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/respect.jpg" class="rounded" alt="...">
                        </a>
                    </div>
                    <div class="col-5 offset-1 px-2">
                        <a href="#!" class="position-relative d-block">
                            <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/pedagogie.jpg" class="rounded" alt="...">
                        </a>
                    </div>
                    <div class="col-5 pl-2">
                        <a href="#!" class="position-relative d-block">
                            <img src="<?php echo base_url(); ?>myprivate/assets/templates/02/img/content/manyvalues2.jpg" class="rounded" alt="..." />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php echo Modules::run('visionmission'); ?>