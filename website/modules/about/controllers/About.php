<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MX_Controller {

	public function __construct(){
		parent::__construct();
		// Your own constructor code
	}

	public function index(){
		$data['metatitle'] = "À propos";
		$data['module'] = "about";
		$data['view'] = "aboutpage";
		echo Modules::run('template/home', $data);
	}

}

?>